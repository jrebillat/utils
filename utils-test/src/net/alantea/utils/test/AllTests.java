package net.alantea.utils.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import net.alantea.utils.test.exception.ExceptionTest;
import net.alantea.utils.test.messages.MessagesTest;

@RunWith(Suite.class)
@SuiteClasses({ ExceptionTest.class,
   MessagesTest.class
   })

public class AllTests
{

}
