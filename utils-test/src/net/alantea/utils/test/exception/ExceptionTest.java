package net.alantea.utils.test.exception;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.utils.exception.LntException;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class ExceptionTest
{
   @Test
   public void T01_simpleExceptionTest()
   {
      LntException except = new LntException("Test it");
      assertNotNull(except);
      
      LntException.LEVEL level = except.getLevel();
      assertEquals(LntException.LEVEL.LOG_WARN, level);
      
      // Dummy clear exceptions
      LntException.popExceptions();
   }
   
   @Test
   public void T02_someExceptionsTest()
   {
      LntException except1 = new LntException("Test it 1");
      assertNotNull(except1);
      LntException except2 = new LntException("Test it 2");
      assertNotNull(except2);
      LntException except3 = new LntException("Test it 3");
      assertNotNull(except3);
      
      List<LntException> list = LntException.popExceptions();
      assertNotNull(list);
      assertFalse(list.isEmpty());
      assertEquals(3, list.size());
      assertEquals(except1, list.get(0));
      assertEquals(except2, list.get(1));
      assertEquals(except3, list.get(2));
   }
   
   @Test
   public void T03_peekExceptionsTest()
   {
      LntException except1 = new LntException("Test it 1");
      assertNotNull(except1);

      List<LntException> list1 = LntException.peekExceptions();
      assertNotNull(list1);
      assertFalse(list1.isEmpty());
      assertEquals(1, list1.size());
      assertEquals(except1, list1.get(0));
      
      LntException except2 = new LntException("Test it 2");
      assertNotNull(except2);

      List<LntException> list2 = LntException.peekExceptions();
      assertNotNull(list2);
      assertFalse(list2.isEmpty());
      assertEquals(2, list2.size());
      assertEquals(except1, list2.get(0));
      assertEquals(except2, list2.get(1));
      
      LntException except3 = new LntException("Test it 3");
      assertNotNull(except3);
      
      List<LntException> list = LntException.popExceptions();
      assertNotNull(list);
      assertFalse(list.isEmpty());
      assertEquals(3, list.size());
      assertEquals(except1, list.get(0));
      assertEquals(except2, list.get(1));
      assertEquals(except3, list.get(2));
   }
}
