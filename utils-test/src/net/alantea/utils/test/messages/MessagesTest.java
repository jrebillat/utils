package net.alantea.utils.test.messages;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import net.alantea.utils.MultiMessages;

public class MessagesTest
{
   @Test
   public void T01_simpleMessageTest()
   {
      MultiMessages.addBundle("net.alantea.utils.test.messages.MessagesTest");
      String text = MultiMessages.get("Encoding.test1");
      assertEquals("éèàçù", text);
   }

}
