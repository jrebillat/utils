package net.alantea.utils;

import net.alantea.utils.exception.LntException;

/**
 * Reflection Utilities.
 */
public final class PrimitiveUtils
{
   
   /**
    * Do not instantiates.
    */
   private PrimitiveUtils()
   {
   }

   /**
    * Test if class is convertible in a primitive type or String.
    *
    * @param clazz the class to analyze
    * @return true, if successful
    */
   public static boolean testPrimitive(Class<?> clazz)
   {
      if (clazz == null)
      {
         return false;
      }
      else if (clazz.isPrimitive())
      {
         return true;
      }
      else if (Number.class.isAssignableFrom(clazz))
      {
         return true;
      }
      else if (Boolean.class.isAssignableFrom(clazz))
      {
         return true;
      }
      else
      {
         return false;
      }
   }

   /**
    * Creates a reserved class instance.
    *
    * @param className the class name
    * @return the object
    */
   public static Object createReserved(String className)
   {
      switch (className)
      {
         case "int":
         case "integer":
            return new Integer(0);
         case "boolean":
            return new Boolean(true);
         case "long":
            return new Long(0);
         case "float":
            return new Float(0);
         case "byte":
            return new Byte((byte) 0);
         case "short":
            return new Short((short) 0);
         case "String":
            return new String("");
         case "string":
             return new String("");
         case "double":
            return new Double(0);
         default:
            return null;
      }
   }

   /**
    * Verify that the class is not reserved.
    *
    * @param className the class name
    * @return true, if successful
    */
   public static boolean verifyNotReserved(String className)
   {
      switch (className)
      {
         case "int":
         case "integer":
         case "boolean":
         case "long":
         case "float":
         case "byte":
         case "short":
         case "String":
         case "string":
         case "double":
            return false;

         default:
            return true;
      }
   }

   /**
    * Verify the class is not a reserved container.
    *
    * @param className the class name
    * @return true, if successful
    */
   public static boolean verifyNotReservedContainer(String className)
   {
      switch (className)
      {
         case "Integer":
         case "Boolean":
         case "Long":
         case "Float":
         case "Byte":
         case "Short":
         case "String":
         case "Double":
            return false;

         default:
            return true;
      }
   }


   /**
    * Gets a simple object from a String.
    *
    * @param targetClass the class for the returned object
    * @param valueText the value as a String
    * @return the simple object parsed from the string, or null
    */
   public static Object getSimpleObject(Class<?> targetClass, String valueText)
   {
      Object value = null;
   try
   {
      if ((Boolean.class.equals(targetClass)) || (Boolean.TYPE.equals(targetClass)))
      {
         value = Boolean.parseBoolean(valueText);
      }
      else if ((Integer.class.equals(targetClass)) || (Integer.TYPE.equals(targetClass)))
      {
         value = Integer.parseInt(valueText);
      }
      else if ((Long.class.equals(targetClass)) || (Long.TYPE.equals(targetClass)))
      {
         value = Long.parseLong(valueText);
      }
      else if ((Float.class.equals(targetClass)) || (Float.TYPE.equals(targetClass)))
      {
         value = Float.parseFloat(valueText);
      }
      else if ((Double.class.equals(targetClass)) || (Double.TYPE.equals(targetClass)))
      {
         value = Double.parseDouble(valueText);
      }
      else if (String.class.equals(targetClass))
      {
         value = valueText;
      }
      else if ((Byte.class.equals(targetClass)) || (Byte.TYPE.equals(targetClass)))
      {
         value = Byte.decode(valueText);
      }
   }
   catch (NumberFormatException ex)
   {
      new LntException("Number format exception for '" + valueText + "'", ex);
      // bad format. Return null.
   }
      return value;
   }

   /**
    * Create a simple object with default value.
    *
    * @param targetClass the class for the returned object
    * @return the simple object parsed from the string, or null
    */
   public static Object getSimpleObjectDefaultValue(Class<?> targetClass)
   {
      Object value = null;
   try
   {
      if ((Boolean.class.equals(targetClass)) || (Boolean.TYPE.equals(targetClass)))
      {
         value = new Boolean(false);
      }
      else if ((Integer.class.equals(targetClass)) || (Integer.TYPE.equals(targetClass)))
      {
         value = new Integer(0);
      }
      else if ((Long.class.equals(targetClass)) || (Long.TYPE.equals(targetClass)))
      {
         value = new Long(0);
      }
      else if ((Float.class.equals(targetClass)) || (Float.TYPE.equals(targetClass)))
      {
         value = new Float(0.0);
      }
      else if ((Double.class.equals(targetClass)) || (Double.TYPE.equals(targetClass)))
      {
         value = new Double(0.0);
      }
      else if (String.class.equals(targetClass))
      {
         value = "";
      }
      else if ((Byte.class.equals(targetClass)) || (Byte.TYPE.equals(targetClass)))
      {
         value = new Byte((byte)0);
      }
   }
   catch (NumberFormatException ex)
   {
      new LntException("Number format exception", ex);
      // bad format. Return null.
   }
      return value;
   }
   
   public static boolean comparePrimitive(Class<?> class1, Class<?> class2)
   {
      if ((class1 == null) || (class2 == null))
      {
         return false;
      }
      else if ((verifyNotReservedContainer(class1.getSimpleName())) && (verifyNotReserved(class1.getSimpleName())))
      {
         return false;
      }
      else if ((verifyNotReservedContainer(class2.getSimpleName())) && (verifyNotReserved(class2.getSimpleName())))
      {
         return false;
      }

      else if (String.class.equals(class2))
      {
         return true;
      }

      else if ((class1.equals(Integer.TYPE) || (class1.equals(Integer.class)))
            && (class2.equals(Integer.TYPE) || (class2.equals(Integer.class))))
      {
         return true;
      }

      else if ((class1.equals(Float.TYPE) || (class1.equals(Float.class)))
            && (class2.equals(Float.TYPE) || (class2.equals(Float.class))))
      {
         return true;
      }

      else if ((class1.equals(Double.TYPE) || (class1.equals(Double.class)))
            && (class2.equals(Double.TYPE) || (class2.equals(Double.class))))
      {
         return true;
      }

      else if ((class1.equals(Long.TYPE) || (class1.equals(Long.class)))
            && (class2.equals(Long.TYPE) || (class2.equals(Long.class))))
      {
         return true;
      }

      else if ((class1.equals(Byte.TYPE) || (class1.equals(Byte.class)))
            && (class2.equals(Byte.TYPE) || (class2.equals(Byte.class))))
      {
         return true;
      }

      else if ((class1.equals(Short.TYPE) || (class1.equals(Short.class)))
            && (class2.equals(Short.TYPE) || (class2.equals(Short.class))))
      {
         return true;
      }

      else if ((class1.equals(Boolean.TYPE) || (class1.equals(Boolean.class)))
            && (class2.equals(Boolean.TYPE) || (class2.equals(Boolean.class))))
      {
         return true;
      }
      
      return false;
   }
   
   public static int getInteger(Object source)
   {
      if (source instanceof String)
      {
         return (int) getSimpleObject(Integer.TYPE, (String)source);
      }
      else
      {
         String buffer = source.toString();
         try
         {
            return Integer.parseInt(buffer);
         }
         catch (NumberFormatException e)
         {
            return 0;
         }
      }
   }
}
