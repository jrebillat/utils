package net.alantea.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import net.alantea.utils.exception.LntException;

/**
 * The Class MessageProvider.
 */
public abstract class MessageProvider
{
   
   /** The methods. */
   private Map<Locale, Method> methods = null;

   /**
    * Instantiates a new message provider.
    */
   public MessageProvider()
   {
   }
   
   /**
    * Gets the.
    *
    * @param key the key
    * @param locale the locale
    * @param args the args
    * @return the string
    */
   public final String get(String key, Locale locale, Object... args)
   {
      if (methods == null)
      {
         methods = new HashMap<>();
         populateMethods();
      }

      String ret = null;
      Method localizedMethod = methods.get(locale);
      if (localizedMethod == null)
      {
         ret = get(key, args);
      }
      else
      {
         try
         {
            ret = (String) localizedMethod.invoke(this, key, (Object[]) args);
         }
         catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
         {
            new LntException("Error accessing method for : " + key, e);
         }
      }
      return ret;
   }

   /**
    * Populate methods.
    */
   private void populateMethods()
   {
      populateMethods(this.getClass());
   }

   /**
    * Populate methods.
    *
    * @param refClass the ref class
    */
   private void populateMethods(Class<?> refClass)
   {
      if (!Object.class.equals(refClass))
      {
         populateMethods(refClass.getSuperclass());
      }
      
      for (Method method : refClass.getDeclaredMethods())
      {
         if (method.getName().matches("^get_[A-Za-z0-9][A-Za-z0-9]_[A-Za-z0-9][A-Za-z0-9]$"))
         {
            if (!Modifier.isStatic(method.getModifiers()))
            {
               if (String.class.equals(method.getReturnType()))
               {
                  if (method.getParameterCount() == 2)
                  {
                     if (method.getParameterCount() == 2)
                     {
                        Class<?>[] pars = method.getParameterTypes();
                        if (String.class.equals(pars[0]))
                        {
                           String name = method.getName();
                           int n = name.length();
                           Locale loc= new Locale(name.substring(n - 5, n - 3), name.substring(n - 2, n));
                           methods.put(loc,  method);
                        }
                     }
                  }
               }
            }
         }
      }
   }

   /**
    * Gets the.
    *
    * @param key the key
    * @param args the args
    * @return the string
    */
   protected abstract String get(String key, Object[] args);

}
