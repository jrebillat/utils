package net.alantea.utils.security;

import java.security.Key;

import javax.crypto.Cipher;


/**
 * The Class Crypt.
 */
public class Crypt
{
   
   /**
    * Encrypt.
    *
    * @param key the key
    * @param toEncode the bytes to encode
    * @return the byte[]
    */
   public static final byte[] encrypt(Key key, byte[] toEncode)
   {
      byte[] output = new byte[0];
      if ((key != null) && (toEncode != null))
      {
        try
        {
            Cipher cipher=Cipher.getInstance(MD5.PROTOCOL_AES);
            cipher.init(Cipher.ENCRYPT_MODE,key);
            output =  cipher.doFinal(output);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
      }
      return output;
   }

   /**
    * Decrypt.
    *
    * @param key the key
    * @param toDecode the bytes to decode
    * @return the byte[]
    */
   public static final byte[] decrypt(Key key, byte[] toDecode)
   {
      byte[] output = new byte[0];
      if ((key != null) && (toDecode != null))
      {
        try
        {
            Cipher cipher=Cipher.getInstance(MD5.PROTOCOL_AES);
            cipher.init(Cipher.DECRYPT_MODE,key);
            output =  cipher.doFinal(output);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
      }
      return output;
   }
}
