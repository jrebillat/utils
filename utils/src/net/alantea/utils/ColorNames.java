package net.alantea.utils;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class ColorNames
{
   public static final String COLOR_PINK = "Pink";
   public static final String COLOR_LIGHTPINK = "LightPink";
   public static final String COLOR_HOTPINK = "HotPink";
   public static final String COLOR_DEEPPINK = "DeepPink";
   public static final String COLOR_PALEVIOLETRED = "PaleVioletRed";
   public static final String COLOR_MEDIUMVIOLETRED = "MediumVioletRed";
   
   public static final String COLOR_LIGHTSALMON = "LightSalmon";
   public static final String COLOR_SALMON = "Salmon";
   public static final String COLOR_DARKSALMON = "DarkSalmon";
   public static final String COLOR_LIGHTCORAL = "LightCoral";
   public static final String COLOR_INDIANRED = "IndianRed";
   public static final String COLOR_CRIMSON = "Crimson";
   public static final String COLOR_FIREBRICK = "Firebrick";
   public static final String COLOR_DARKRED = "DarkRed";
   public static final String COLOR_RED = "Red";
   
   public static final String COLOR_ORANGERED = "OrangeRed";
   public static final String COLOR_TOMATO = "Tomato";
   public static final String COLOR_CORAL = "Coral";
   public static final String COLOR_DARKORANGE = "DarkOrange";
   public static final String COLOR_ORANGE = "Orange";

   public static final String COLOR_YELLOW = "Yellow";
   public static final String COLOR_LIGHTYELLOW = "LightYellow";
   public static final String COLOR_LEMONCHIFFON = "LemonChiffon";
   public static final String COLOR_LIGHTGOLDENRODYELLOW = "LightGoldenrodYellow";
   public static final String COLOR_PAPAYAWHIP = "PapayaWhip";
   public static final String COLOR_MOCASSIN = "Moccasin";
   public static final String COLOR_PEACHPUFF = "PeachPuff";
   public static final String COLOR_PALEGOLDENROD = "PaleGoldenrod";
   public static final String COLOR_KHAKI = "Khaki";
   public static final String COLOR_DARKKHAKI = "DarkKhaki";
   public static final String COLOR_GOLD = "Gold";

   public static final String COLOR_CORNSILK = "CornSilk";
   public static final String COLOR_BLANCHEDALMOND = "BlanchedAlmond";
   public static final String COLOR_BISQUE = "Bisque";
   public static final String COLOR_NAVAJOWHITE = "NavajoWhite";
   public static final String COLOR_WHEAT = "Wheat";
   public static final String COLOR_BURLYWOOD = "Burlywood";
   public static final String COLOR_TAN = "Tan";
   public static final String COLOR_ROSYBROWN = "RosyBrown";
   public static final String COLOR_SANDYBROWN = "SandyBrown";
   public static final String COLOR_GOLDENROD = "Goldenrod";
   public static final String COLOR_DARKGOLDENROD = "DarkGoldenrod";
   public static final String COLOR_PERU = "Peru";
   public static final String COLOR_CHOCOLATE = "Chocolate";
   public static final String COLOR_SADDLEBROWN = "SaddleBrown";
   public static final String COLOR_SIENNA = "Sienna";
   public static final String COLOR_BROWN = "Brown";
   public static final String COLOR_MAROON = "Maroon";

   public static final String COLOR_LAVENDER = "Lavender";
   public static final String COLOR_THISTLE = "Thistle";
   public static final String COLOR_PLUM = "Plum";
   public static final String COLOR_VIOLET = "Violet";
   public static final String COLOR_ORCHID = "Orchid";
   public static final String COLOR_FUCHSIA = "Fuchsia";
   public static final String COLOR_MAGENTA = "Magenta";
   public static final String COLOR_MEDIUMORCHID = "MediumOrchid";
   public static final String COLOR_MEDIUMPURPLE = "MediumPurple";
   public static final String COLOR_BLUEVIOLET = "BlueViolet";
   public static final String COLOR_DARKVIOLET = "DarkViolet";
   public static final String COLOR_DARKORCHID = "DarkOrchid";
   public static final String COLOR_DARKMAGENTA = "DarkMagenta";
   public static final String COLOR_PURPLE = "Purple";
   public static final String COLOR_INDIGO = "Indigo";
   public static final String COLOR_DARKSLATEBLUE = "DarkSlateBlue";
   public static final String COLOR_SLATEBLUE = "SlateBlue";
   public static final String COLOR_MEDIUMSLATEBLUE = "MediumSlateBlue";

   public static final String COLOR_WHITE = "White";
   public static final String COLOR_SNOW = "Snow";
   public static final String COLOR_HONEYDEW = "Honeydew";
   public static final String COLOR_MINTCREAM = "MintCream";
   public static final String COLOR_AZURE = "Azure";
   public static final String COLOR_ALICEBLUE = "AliceBlue";
   public static final String COLOR_GHOSTWHITE = "GhostWhite";
   public static final String COLOR_WHITESMOKE = "WhiteSmoke";
   public static final String COLOR_SEASHELL = "Seashell";
   public static final String COLOR_BEIGE = "Beige";
   public static final String COLOR_OLDLACE = "OldLace";
   public static final String COLOR_FLORALWHITE = "FloralWhite";
   public static final String COLOR_IVORY = "Ivory";
   public static final String COLOR_ANTIQUEWHITE = "AntiqueWhite";
   public static final String COLOR_LINEN = "Linen";
   public static final String COLOR_LAVENDERBLUSH = "LavenderBlush";
   public static final String COLOR_MISTYROSE = "MistyRose";

   public static final String COLOR_GAINSBORO = "Gainsboro";
   public static final String COLOR_LIGHTGRAY = "LightGray";
   public static final String COLOR_SILVER = "Silver";
   public static final String COLOR_DARKGRAY = "DarkGray";
   public static final String COLOR_GRAY = "Gray";
   public static final String COLOR_DIMGRAY = "DimGray";
   public static final String COLOR_LIGHTSLATEGRAY = "LightSlateGray";
   public static final String COLOR_SLATEGRAY = "SlateGray";
   public static final String COLOR_DARKSLATEGRAY = "DarkSlateGray";
   public static final String COLOR_BLACK = "Black";

   public static final String COLOR_DARKOLIVEGREEN = "DarkOliveGreen";
   public static final String COLOR_OLIVE = "Olive";
   public static final String COLOR_OLIVEDRAB = "OliveDrab";
   public static final String COLOR_YELLOWGREEN = "YellowGreen";
   public static final String COLOR_LIMEGREEN = "LimeGreen";
   public static final String COLOR_LIME = "Lime";
   public static final String COLOR_LAWNGREEN = "LawnGreen";
   public static final String COLOR_CHARTREUSE = "Chartreuse";
   public static final String COLOR_GREENYELLOW = "GreenYellow";
   public static final String COLOR_SPRINGGREEN = "SpringGreen";
   public static final String COLOR_MEDIUMSPRINGGREEN = "MediumSpringGreen";
   public static final String COLOR_LIGHTGREEN = "LightGreen";
   public static final String COLOR_PALEGREEN = "PaleGreen";
   public static final String COLOR_DARKSEAGREEN = "DarkSeaGreen";
   public static final String COLOR_MEDIUMAQUAMARINE = "MediumAquamarine";
   public static final String COLOR_MEDIUMSEAGREEN = "MediumSeaGreen";
   public static final String COLOR_SEAGREEN = "SeaGreen";
   public static final String COLOR_FORESTGREEN = "ForestGreen";
   public static final String COLOR_GREEN = "Green";
   public static final String COLOR_DARKGREEN = "DarkGreen";

   public static final String COLOR_AQUA = "Aqua";
   public static final String COLOR_CYAN = "Cyan";
   public static final String COLOR_LIGHTCYAN = "LightCyan";
   public static final String COLOR_PALETURQUOISE = "PaleTurquoise";
   public static final String COLOR_AQUAMARINE = "Aquamarine";
   public static final String COLOR_TURQUOISE = "Turquoise";
   public static final String COLOR_MEDIUMTURQUOISE = "MediumTurquoise";
   public static final String COLOR_DARKTURQUOISE = "DarkTurquoise";
   public static final String COLOR_LIGHTSEAGREEN = "LightSeaGreen";
   public static final String COLOR_CADETBLUE = "CadetBlue";
   public static final String COLOR_DARKCYAN = "DarkCyan";
   public static final String COLOR_TEAL = "Teal";
   
   public static final String COLOR_LIGHTSTEELBLUE = "LightSteelBlue";
   public static final String COLOR_POWDERBLUE = "PowderBlue";
   public static final String COLOR_LIGHTBLUE = "LightBlue";
   public static final String COLOR_SKYBLUE = "SkyBlue";
   public static final String COLOR_LIGHTSKYBLUE = "LightSkyBlue";
   public static final String COLOR_DEEPSKYBLUE = "DeepSkyBlue";
   public static final String COLOR_DODGERBLUE = "DodgerBlue";
   public static final String COLOR_CORNFLOWERBLUE = "CornflowerBlue";
   public static final String COLOR_STEELBLUE = "SteelBlue";
   public static final String COLOR_ROYALBLUE = "RoyalBlue";
   public static final String COLOR_BLUE = "Blue";
   public static final String COLOR_MEDIUMBLUE = "MediumBlue";
   public static final String COLOR_DARKBLUE = "DarkBlue";
   public static final String COLOR_NAVY = "Navy";
   public static final String COLOR_MIDNIGHTBLUE = "MidnightBlue";
   
   private static Map<String, String> colorMap = new HashMap<String, String>() {
      private static final long serialVersionUID = 1L;

   {
      put(COLOR_PINK, "#FFC0CB");
      put(COLOR_LIGHTPINK, "#FFB6C1");
      put(COLOR_HOTPINK, "#FF69B4");
      put(COLOR_DEEPPINK, "#FF1493");
      put(COLOR_PALEVIOLETRED, "#DB7093");
      put(COLOR_MEDIUMVIOLETRED, "#C71585");
      
      put(COLOR_LIGHTSALMON, "#FFA07A");
      put(COLOR_SALMON, "#FA8072");
      put(COLOR_DARKSALMON, "#E9967A");
      put(COLOR_LIGHTCORAL, "#F08080");
      put(COLOR_INDIANRED, "#CD5C5C");
      put(COLOR_CRIMSON, "#DC143C");
      put(COLOR_FIREBRICK, "#B22222");
      put(COLOR_DARKRED, "#8B0000");
      put(COLOR_RED, "#FF0000");
      
      put(COLOR_ORANGERED, "#FF4500");
      put(COLOR_TOMATO, "#FF6347");
      put(COLOR_CORAL, "#FF7F50");
      put(COLOR_DARKORANGE, "#FF8C00");
      put(COLOR_ORANGE, "#FFA500");

      put(COLOR_YELLOW, "#FFFF00");
      put(COLOR_LIGHTYELLOW, "#FFFFE0");
      put(COLOR_LEMONCHIFFON, "#FFFACD");
      put(COLOR_LIGHTGOLDENRODYELLOW, "#FAFAD2");
      put(COLOR_PAPAYAWHIP, "#FFEFD5");
      put(COLOR_MOCASSIN, "#FFE4B5");
      put(COLOR_PEACHPUFF, "#FFDAB9");
      put(COLOR_PALEGOLDENROD, "#EEE8AA");
      put(COLOR_KHAKI, "#F0E68C");
      put(COLOR_DARKKHAKI, "#BDB76B");
      put(COLOR_GOLD, "#FFD700");

      put(COLOR_CORNSILK, "#FFF8DC");
      put(COLOR_BLANCHEDALMOND, "#FFEBCD");
      put(COLOR_BISQUE, "#FFDEAD");
      put(COLOR_NAVAJOWHITE, "#FFD700");
      put(COLOR_WHEAT, "#F5DEB3");
      put(COLOR_BURLYWOOD, "#DEB887");
      put(COLOR_TAN, "#D2B48C");
      put(COLOR_ROSYBROWN, "#BC8F8F");
      put(COLOR_SANDYBROWN, "#F4A460");
      put(COLOR_GOLDENROD, "#DAA520");
      put(COLOR_DARKGOLDENROD, "#B8860B");
      put(COLOR_PERU, "#CD853F");
      put(COLOR_CHOCOLATE, "#D2691E");
      put(COLOR_SADDLEBROWN, "#8B4513");
      put(COLOR_SIENNA, "#A0522D");
      put(COLOR_BROWN, "#A52A2A");
      put(COLOR_MAROON, "#800000");

      put(COLOR_LAVENDER, "#E6E6FA");
      put(COLOR_THISTLE, "#D8BFD8");
      put(COLOR_PLUM, "#DDA0DD");
      put(COLOR_VIOLET, "#EE82EE");
      put(COLOR_ORCHID, "#DA70D6");
      put(COLOR_FUCHSIA, "#FF00FF");
      put(COLOR_MAGENTA, "#FF00FF");
      put(COLOR_MEDIUMORCHID, "#BA55D3");
      put(COLOR_MEDIUMPURPLE, "#9370DB");
      put(COLOR_BLUEVIOLET, "#8A2BE2");
      put(COLOR_DARKVIOLET, "#9400D3");
      put(COLOR_DARKORCHID, "#9932CC");
      put(COLOR_DARKMAGENTA, "#8B008B");
      put(COLOR_PURPLE, "#800080");
      put(COLOR_INDIGO, "#4B0082");
      put(COLOR_DARKSLATEBLUE, "#483D8B");
      put(COLOR_SLATEBLUE, "#6A5ACD");
      put(COLOR_MEDIUMSLATEBLUE, "#7B68EE");

      put(COLOR_WHITE, "#FFFFFF");
      put(COLOR_SNOW, "#FFFAFA");
      put(COLOR_HONEYDEW, "#F0FFF0");
      put(COLOR_MINTCREAM, "#F5FFFA");
      put(COLOR_AZURE, "#F0FFFF");
      put(COLOR_ALICEBLUE, "#F0F8FF");
      put(COLOR_GHOSTWHITE, "F8F8FF");
      put(COLOR_WHITESMOKE, "#F5F5F5");
      put(COLOR_SEASHELL, "#FFF5EE");
      put(COLOR_BEIGE, "#F5F5DC");
      put(COLOR_OLDLACE, "#FDF5E6");
      put(COLOR_FLORALWHITE, "#FFFAF0");
      put(COLOR_IVORY, "#FFFFF0");
      put(COLOR_ANTIQUEWHITE, "#FAEBD7");
      put(COLOR_LINEN, "#FAF0E6");
      put(COLOR_LAVENDERBLUSH, "#FFF0F5");
      put(COLOR_MISTYROSE, "#FFE4E1");

      put(COLOR_GAINSBORO, "#DCDCDC");
      put(COLOR_LIGHTGRAY, "#D3D3D3");
      put(COLOR_SILVER, "#C0C0C0");
      put(COLOR_DARKGRAY, "#A9A9A9");
      put(COLOR_GRAY, "#808080");
      put(COLOR_DIMGRAY, "#696969");
      put(COLOR_LIGHTSLATEGRAY, "#778899");
      put(COLOR_SLATEGRAY, "#708090");
      put(COLOR_DARKSLATEGRAY, "#2F4F4F");
      put(COLOR_BLACK, "#000000");

      put(COLOR_DARKOLIVEGREEN, "#556B2F");
      put(COLOR_OLIVE, "#808000");
      put(COLOR_OLIVEDRAB, "#6B8E23");
      put(COLOR_YELLOWGREEN, "#9ACD32");
      put(COLOR_LIMEGREEN, "#32CD32");
      put(COLOR_LIME, "#00FF00");
      put(COLOR_LAWNGREEN, "#7CFC00");
      put(COLOR_CHARTREUSE, "#7FFF00");
      put(COLOR_GREENYELLOW, "#ADFF2F");
      put(COLOR_SPRINGGREEN, "#00FF7F");
      put(COLOR_MEDIUMSPRINGGREEN, "#00FA9A");
      put(COLOR_LIGHTGREEN, "#90EE90");
      put(COLOR_PALEGREEN, "#98FB98");
      put(COLOR_DARKSEAGREEN, "#8FBC8F");
      put(COLOR_MEDIUMAQUAMARINE, "#66 CD AA");
      put(COLOR_MEDIUMSEAGREEN, "#3C B3 71");
      put(COLOR_SEAGREEN, "#2E8B57");
      put(COLOR_FORESTGREEN, "#228B22");
      put(COLOR_GREEN, "#008000");
      put(COLOR_DARKGREEN, "#006400");

      put(COLOR_AQUA, "#00FFFF");
      put(COLOR_CYAN, "#00FFFF");
      put(COLOR_LIGHTCYAN, "#E0FFFF");
      put(COLOR_PALETURQUOISE, "#AFEEEE");
      put(COLOR_AQUAMARINE, "#7FFFD4");
      put(COLOR_TURQUOISE, "#40E0D0");
      put(COLOR_MEDIUMTURQUOISE, "#48D1CC");
      put(COLOR_DARKTURQUOISE, "#00CED1");
      put(COLOR_LIGHTSEAGREEN, "#20B2AA");
      put(COLOR_CADETBLUE, "#5F9EA0");
      put(COLOR_DARKCYAN, "#008B8B");
      put(COLOR_TEAL, "#008080");
      
      put(COLOR_LIGHTSTEELBLUE, "#B0C4DE");
      put(COLOR_POWDERBLUE, "#B0E0E6");
      put(COLOR_LIGHTBLUE, "#ADD8E6");
      put(COLOR_SKYBLUE, "#87CEEB");
      put(COLOR_LIGHTSKYBLUE, "#87CEFA");
      put(COLOR_DEEPSKYBLUE, "#00BFFF");
      put(COLOR_DODGERBLUE, "#1E90FF");
      put(COLOR_CORNFLOWERBLUE, "#6495ED");
      put(COLOR_STEELBLUE, "#4682B4");
      put(COLOR_ROYALBLUE, "#4169E1");
      put(COLOR_BLUE, "#0000FF");
      put(COLOR_MEDIUMBLUE, "#0000CD");
      put(COLOR_DARKBLUE, "#00008B");
      put(COLOR_NAVY, "#000080");
      put(COLOR_MIDNIGHTBLUE, "#191970");
  }};
  
  private final static List<String> defaultNames = new ArrayList<>(colorMap.keySet());
  
  /**
   * Add the color.
   *
   * @param colorName the color name
   * @param webCode the web code starting with # followed by 6 hexadecimal digits
   */
  public static void addColor(String colorName, String webCode)
  {
     if ((colorName != null) && (!colorName.isEmpty()) && (!colorMap.containsKey(colorName)))
     {
        String theCode = webCode.trim().toUpperCase();
        if ((theCode != null) && (!theCode.isEmpty())
              && ((theCode.matches("^#[0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F]$"))
              || (theCode.matches("^#[0-9A-F][0-9A-F][0-9A-F]$"))))
        {
           colorMap.put(colorName, webCode);
        }
     }
  }
  
  /**
   * Change the color.
   *
   * @param colorName the color name
   * @param webCode the new web code starting with # followed by 6 hexadecimal digits
   */
  public static void changeColor(String colorName, String webCode)
  {
     if ((colorName != null) && (!colorName.isEmpty()))
     {
        if ((!defaultNames.contains(colorName)) && (colorMap.containsKey(colorName)))
        {
           String theCode = webCode.trim().toUpperCase();
           if ((theCode != null) && (!theCode.isEmpty())
                 && ((theCode.matches("^#[0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F]$"))
                       || (theCode.matches("^#[0-9A-F][0-9A-F][0-9A-F]$"))))
           {
              colorMap.put(colorName, webCode);
           }
        }
     }
  }
  
  /**
   * Verify if color exists.
   *
   * @param colorName the color name
   * @return true if the color is known.
   */
  public static boolean exists(String colorName)
  {
     if ((colorName.toUpperCase().matches("^#[0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F]"))
        || (colorName.toUpperCase().matches("^#[0-9A-F][0-9A-F][0-9A-F]")))
     {
        return true;
     }
     
     String ret = colorMap.get(colorName);
     return (ret != null);
  }
  
  /**
   * Gets the color code.
   *
   * @param colorName the color name
   * @return the color code or "#000000"
   */
  public static String getColorCode(String colorName)
  {
     String ret;
     if ((colorName.toUpperCase().matches("^#[0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F]"))
           || (colorName.toUpperCase().matches("^#[0-9A-F][0-9A-F][0-9A-F]")))
     {
        ret = colorName;
     }
     ret = colorMap.get(colorName);
     return (ret == null) ? "#000000" : ret;
  }
  
  /**
   * Gets the color names.
   *
   * @return the color names
   */
  public static List<String> getColorNames()
  {
     return new ArrayList<>(colorMap.keySet());
  }

  /**
   * Gets the AWT color.
   *
   * @param colorName the color name
   * @return the color
   */
  public static Color getColor(String colorName)
  {
     if (!exists(colorName))
     {
        return null;
     }
     else if (colorName.startsWith("#"))
     {
        return Color.decode(colorName);
     }
     else
     {
        return Color.decode(ColorNames.getColorCode(colorName));
     }
  }

  /**
   * Gets the color as RGB integer.
   *
   * @param color the AWT color
   * @return the RGB int value
   */
  public static int getRGB(Color color)
  {
     return color.getRGB();
  }

  /**
   * Gets the color as RGB HTML code.
   *
   * @param color the AWT color
   * @return the RGB hex value with preceding #
   */
  public static String getRGBCode(Color color)
  {
     if (color == null)
     {
        return "#000000";
     }
     int red = color.getRed();
     int green = color.getGreen();
     int blue = color.getBlue();
     return String.format("#%02x%02x%02x", red, green, blue);
  }

}
