package net.alantea.utils;

public final class ByteUtils
{

   private ByteUtils()
   {
   }

   /**
   * Get an integer value from four bytes.
   *
   * @param array of bytes to parse
   * @return the value
   */
   public static int readNumber(byte[] array)
   {
      // read number of bytes
      int nbbytes;
      // the size for the buffer is on 4 bytes
      nbbytes = getUnsignedByte(array, 0) * 256 * 256 * 256;
      nbbytes += getUnsignedByte(array, 1) * 256 * 256;
      nbbytes += getUnsignedByte(array, 2) * 256;
      nbbytes += getUnsignedByte(array, 3);

      return nbbytes;
   }

   /**
    * Gets the unsigned byte.
    *
    * @param array the array
    * @param index the index
    * @return the unsigned byte
    */
   private static int getUnsignedByte(byte[] array, int index)
   {
      if ((array == null) || (array.length == 0))
      {
         return 0;
      }
      
      int n = array[index];
      if (n < 0)
      {
         n += 256;
      }
      return n;
   }

   /**
    * get four bytes from an integer value
    * 
    * @param val to cut
    * @return four bytes
    */
   public static byte[] getBytesForInt(int val)
   {
      int n = val;
      // construct size information
      int n3 = n / (256 * 256 * 256);
      n -= n3 * (256 * 256 * 256);
      int n2 = n / (256 * 256);
      n -= n2 * (256 * 256);
      int n1 = n / (256);
      n -= n3 * (256);

      byte[] ret = new byte[4];
      ret[0] = (byte) n3;
      ret[1] = (byte) n2;
      ret[2] = (byte) n1;
      ret[3] = (byte) n;
      return ret;
   }
}
