package net.alantea.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class I18N 
{
   private I18N() {}
   
   public static String getCountryCode()
   {
      return getCountryCode(MultiMessages.getLocale());
   }
   
   public static String getCountryCode(Locale locale)
   {
      return locale.getLanguage() + "_" + locale.getCountry();
   }
   
   public static String getDisplayCountry(Locale locale)
   {
      return locale.getDisplayCountry();
   }
   
   public static final String getDisplayCountry()
   {
      return MultiMessages.getLocale().getDisplayCountry();
   }
	
	public static String getDate(Date date)
	{
		return getDate(date, DateFormat.DEFAULT);
	}
	
	public static String getShortDate(Date date)
	{
		return getDate(date, DateFormat.SHORT);
	}
	
	public static String getMediumDate(Date date)
	{
		return getDate(date, DateFormat.MEDIUM);
	}
	
	public static String getLongDate(Date date)
	{
		return getDate(date, DateFormat.LONG);
	}
   
   public static String getFullDate(Date date)
   {
      return getDate(date, DateFormat.FULL);
   }

	private static String getDate(Date date, int dateFormat)
	{
		DateFormat format = DateFormat.getDateInstance(dateFormat);
		return format.format(date);
	}
	
   public static String getTime(Date date)
   {
      return getTime(date, DateFormat.DEFAULT);
   }
   
   public static String getShortTime(Date date)
   {
      return getTime(date, DateFormat.SHORT);
   }
   
   public static String getMediumTime(Date date)
   {
      return getTime(date, DateFormat.MEDIUM);
   }
   
   public static String getLongTime(Date date)
   {
      return getTime(date, DateFormat.LONG);
   }
   
   public static String getFullTime(Date date)
   {
      return getTime(date, DateFormat.FULL);
   }

   public static String getTime(Date date, int dateFormat)
   {
      DateFormat format = DateFormat.getTimeInstance(dateFormat);
      return format.format(date);
   }
   
   public static String getDateTime(Date date)
   {
      return getDateTime(date, DateFormat.DEFAULT, DateFormat.DEFAULT);
   }
   
   public static String getShortDateTime(Date date)
   {
      return getDateTime(date, DateFormat.SHORT, DateFormat.SHORT);
   }
   
   public static String getMediumDateTime(Date date)
   {
      return getDateTime(date, DateFormat.MEDIUM, DateFormat.MEDIUM);
   }
   
   public static String getLongDateTime(Date date)
   {
      return getDateTime(date, DateFormat.LONG, DateFormat.LONG);
   }
   
   public static String getFullDateTime(Date date)
   {
      return getDateTime(date, DateFormat.FULL, DateFormat.FULL);
   }

   public static String getDateTime(Date date, int dateFormat, int timeFormat)
   {
      DateFormat format = DateFormat.getDateTimeInstance(dateFormat, timeFormat, MultiMessages.getLocale());
      return format.format(date);
   }

   public static String getDateTime(Date date,String formatString)
   {
      DateFormat format = new SimpleDateFormat(formatString);
      return format.format(date);
   }

	public static final String get(String resourceKey, String... args)
	{
		return MultiMessages.get(resourceKey, (Object[])args);
	}
}
