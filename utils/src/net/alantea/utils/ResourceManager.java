/*******************************************************************************
 * Copyright (c) 2011 Google, Inc.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Google, Inc. - initial API and implementation
 *******************************************************************************/
package net.alantea.utils;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 * Utility class for managing OS resources associated with SWT controls such as colors, fonts,
 * images, etc.
 * <p>
 * !!! IMPORTANT !!! Application code must explicitly invoke the <code>dispose()</code> method to
 * release the operating system resources managed by cached objects when those objects and OS
 * resources are no longer needed (e.g. on application shutdown)
 * <p>
 * This class may be freely distributed as part of any application or plugin.
 * <p>
 * 
 * @author scheglov_ke
 * @author Dan Rubel
 */
public class ResourceManager
{
   ////////////////////////////////////////////////////////////////////////////
   //
   // Color
   //
   ////////////////////////////////////////////////////////////////////////////

   ////////////////////////////////////////////////////////////////////////////
   //
   // Image
   //
   ////////////////////////////////////////////////////////////////////////////

   /**
    * Maps image paths to images.
    */
   private static Map<String, Image> m_imageMap = new HashMap<>();
   
   /**
    * Store image.
    *
    * @param name the name
    * @param image the image
    */
   public static void storeImage(String name, Image image)
   {
      m_imageMap.put(name, image);
   }

   /**
    * Returns an {@link Image} encoded by the specified {@link InputStream}.
    *
    * @param stream the {@link InputStream} encoding the image data
    * @return the {@link Image} encoded by the specified input stream
    * @throws IOException Signals that an I/O exception has occurred.
    */
   protected static Image getImage(InputStream stream) throws IOException
   {
      try
      {
        return  ImageIO.read(stream);
      }
      finally
      {
         stream.close();
      }
   }

   /**
    * Returns an {@link Image} stored in the file at the specified path.
    * 
    * @param path the path to the image file
    * @return the {@link Image} stored in the file at the specified path
    */
   public static Image getImage(String path)
   {
      Image image = m_imageMap.get(path);
      if (image == null)
      {
         if (new File(path).exists())
         {
            try
            {
               image = getImage(new FileInputStream(path));
            }
            catch (Exception e)
            {
               image = getMissingImage();
            }
         }
         else
         {
            try
            {
               URL res = ResourceManager.class.getResource("/images/" + path);
               if (res != null)
               {
                  image = ImageIO.read(res);
               }
               else
               {
                  image = getMissingImage();
               }
            }
            catch (IOException e)
            {
               image = getMissingImage();
               e.printStackTrace();
            }
         }
         m_imageMap.put(path, image);
      }
      return image;
   }

   /**
    * Returns an {@link Image} stored in the file at the specified path relative to the specified
    * class.
    * 
    * @param clazz the {@link Class} relative to which to find the image
    * @param path the path to the image file, if starts with <code>'/'</code>
    * @return the {@link Image} stored in the file at the specified path
    */
   public static Image getImage(Class<?> clazz, String path)
   {
      String key = clazz.getName() + '|' + path;
      Image image = m_imageMap.get(key);
      if (image == null)
      {
         try
         {
            image = getImage(clazz.getResourceAsStream(path));
            m_imageMap.put(key, image);
         }
         catch (Exception e)
         {
            image = getMissingImage();
            m_imageMap.put(key, image);
         }
      }
      return image;
   }
   
   /**
    * Gets the icon.
    *
    * @param path the path
    * @return the icon
    */
   public static ImageIcon getIcon(String path)
   {
      return new ImageIcon(getImage(path));
   }
   
   /**
    * Gets the icon.
    *
    * @param path the path
    * @param width the width
    * @return the icon
    */
   public static ImageIcon getIcon(String path, int width)
   {
      Image image = getImage((BufferedImage)getImage(path), width);
      return new ImageIcon(image);
   }
   
   /**
    * Gets the icon.
    *
    * @param image the image
    * @return the icon
    */
   public static ImageIcon getIcon(Image image)
   {
      return (image == null) ? null : new ImageIcon(image);
   }

   private static final int MISSING_IMAGE_SIZE = 10;

   /**
    * @return the small {@link Image} that can be used as placeholder for missing image.
    */
   private static Image getMissingImage()
   {
      BufferedImage image = new BufferedImage(MISSING_IMAGE_SIZE, MISSING_IMAGE_SIZE, BufferedImage.TYPE_INT_RGB);
      
      Graphics2D gc = image.createGraphics();
      gc.setBackground(Color.BLACK);
      gc.fillRect(0, 0, MISSING_IMAGE_SIZE, MISSING_IMAGE_SIZE);
      gc.dispose();
      
      return image;
   }

   @SuppressWarnings("restriction")
   public static BufferedImage getImage(BufferedImage image, int width)
   {
      int w = image.getWidth();
      int h = image.getHeight();
      if (w == width)
      {
         return image;
      }
      Image img = image.getScaledInstance(width,
            (int)(width * h / ((double)w)),
            Image.SCALE_DEFAULT);
      return ((sun.awt.image.ToolkitImage) img).getBufferedImage();
   }

   ////////////////////////////////////////////////////////////////////////////
   //
   // Font
   //
   ////////////////////////////////////////////////////////////////////////////
   /**
    * Maps font names to fonts.
    */
   private static Map<String, Font> m_fontMap = new HashMap<String, Font>();
   /**
    * Maps fonts to their bold versions.
    */
   private static Map<Font, Font> m_fontToBoldFontMap = new HashMap<Font, Font>();
   /**
    * Maps fonts to their italic versions.
    */
   private static Map<Font, Font> m_fontToItalicFontMap = new HashMap<Font, Font>();
   /**
    * Maps fonts to their bold italic versions.
    */
   private static Map<Font, Font> m_fontToBoldItalicFontMap = new HashMap<Font, Font>();

   /**
    * Returns a {@link Font} based on its name, height and style.
    * 
    * @param name the name of the font
    * @param height the height of the font
    * @param style the style of the font
    * @return {@link Font} The font matching the name, height and style
    */
   public static Font getFont(String name, int height, int style)
   {
      String fontName = name + '|' + height + '|' + style;
      Font font = m_fontMap.get(fontName);
      if (font == null)
      {
         font = new Font(name, style, height);
         m_fontMap.put(fontName, font);
      }
      return font;
   }

   /**
    * Returns a bold version of the given {@link Font}.
    * 
    * @param baseFont the {@link Font} for which a bold version is desired
    * @return the bold version of the given {@link Font}
    */
   public static Font getBoldFont(Font baseFont)
   {
      Font font = m_fontToBoldFontMap.get(baseFont);
      if (font == null)
      {
         font = new Font(baseFont.getName(), Font.BOLD, baseFont.getSize());
         m_fontToBoldFontMap.put(baseFont, font);
      }
      return font;
   }

   /**
    * Returns an italic version of the given {@link Font}.
    * 
    * @param baseFont the {@link Font} for which a bold version is desired
    * @return the italic version of the given {@link Font}
    */
   public static Font getItalicFont(Font baseFont)
   {
      Font font = m_fontToItalicFontMap.get(baseFont);
      if (font == null)
      {
         font = new Font(baseFont.getName(), Font.ITALIC, baseFont.getSize());
         m_fontToItalicFontMap.put(baseFont, font);
      }
      return font;
   }

   /**
    * Returns a bold and italic version of the given {@link Font}.
    * 
    * @param baseFont the {@link Font} for which a bold version is desired
    * @return the bold and italic version of the given {@link Font}
    */
   public static Font getBoldItalicFont(Font baseFont)
   {
      Font font = m_fontToBoldItalicFontMap.get(baseFont);
      if (font == null)
      {
         font = new Font(baseFont.getName(), Font.ITALIC + Font.BOLD, baseFont.getSize());
         m_fontToBoldItalicFontMap.put(baseFont, font);
      }
      return font;
   }

   ////////////////////////////////////////////////////////////////////////////
   //
   // Cursor
   //
   ////////////////////////////////////////////////////////////////////////////
   /**
    * Maps IDs to cursors.
    */
   private static Map<Integer, Cursor> m_idToCursorMap = new HashMap<Integer, Cursor>();

   /**
    * Returns the system cursor matching the specific ID.
    * 
    * @param id int The ID value for the cursor
    * @return Cursor The system cursor matching the specific ID
    */
   public static Cursor getCursor(int id)
   {
      Integer key = Integer.valueOf(id);
      Cursor cursor = m_idToCursorMap.get(key);
      if (cursor == null)
      {
         cursor = new Cursor(Cursor.DEFAULT_CURSOR);
         m_idToCursorMap.put(key, cursor);
      }
      return cursor;
   }
}