package net.alantea.utils;

import java.awt.Image;
import java.awt.image.BufferedImage;

/**
 * The Class Images.
 */
public final class Images
{
   
   /**
    * Instantiates a new images.
    */
   private Images() {}
   
   /**
    * Resize image to height.
    *
    * @param original the original
    * @param maxh the maxh
    * @return the image
    */
   public static Image resizeImageToHeight(BufferedImage original, int maxh)
   {
      if (original == null) return null;
      int maxw = original.getWidth() * maxh / original.getHeight();
      return resizeImage(original, maxw, maxh);
   }
   
   /**
    * Resize image to width.
    *
    * @param original the original
    * @param maxw the maxw
    * @return the image
    */
   public static Image resizeImageToWidth(BufferedImage original, int maxw)
   {
      if (original == null) return null;
      int maxh = original.getHeight() * maxw / original.getWidth();
      return resizeImage(original, maxw, maxh);
   }
   
   /**
    * Resize image.
    *
    * @param original the original
    * @param maxw the maxw
    * @param maxh the maxh
    * @return the image
    */
   public static Image resizeImage(BufferedImage original, int maxw, int maxh)
   {
      Image image = original;
      if (image == null) return null;
      int w = maxw -5;
      int h = maxh -5;
      if (original.getWidth()/ maxw > original.getHeight() / maxh)
      {
         h = original.getHeight() * maxw / original.getWidth();
      }
      else
      {
         w = original.getWidth() * maxh / original.getHeight();
      }
      if (w > maxw - 5)
      {
         h = h * (maxw - 5) / w;
      }
      else if (h > maxh - 5)
      {
         w = w * (maxh - 5) / h;
      }
      image = (Image) original.getScaledInstance(w, h, Image.SCALE_DEFAULT);
      return image;
   }
}
