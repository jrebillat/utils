package net.alantea.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class StringUtils.
 */
public class StringUtils
{
   /**
    * Intelligent split keeping elements inside double quotes.
    *
    * @param original the original
    * @return the string[]
    */
   public static String[] intelligentSplit(String original)
   {
      String[] ret = new String[0];
      if (original != null)
      {
         if (original.trim().isEmpty())
         {
            // do nothing
         }
         else if (original.contains("\""))
         {
            String toLookIn = original.trim();
            List<String> list = new LinkedList<String>();
            Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(toLookIn);
            while (m.find())
            {
               list.add(m.group(1).replace("\"", ""));
            }
            ret = list.toArray(new String[0]);
         }
         else
         {
            ret = original.split("\\s+");
         }
      }
      return ret;
   }
}
