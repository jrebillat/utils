package net.alantea.utils;

/**
 * The Class LocalizedString.
 */
public class LocalizedString implements Comparable<Object>
{
   
   /** The key. */
   String _key;
   
   /** The text. */
   String _text;
   
   /**
    * Instantiates a new localized string.
    *
    * @param key the key
    * @param args the args
    */
   public LocalizedString(String key, String... args)
   {
      _key = key;
      _text = MultiMessages.get(key, (Object[])args);
   }
   
   /* (non-Javadoc)
    * @see java.lang.Object#toString()
    */
   @Override
   public String toString()
   {
      return _text;
   }
   
   /**
    * Gets the key.
    *
    * @return the key
    */
   public String getKey()
   {
      return _key;
   }
   
   /* (non-Javadoc)
    * @see java.lang.Object#equals(java.lang.Object)
    */
   @Override
   public boolean equals(Object object)
   {
      if (object instanceof LocalizedString)
      {
         return _key.equals(((LocalizedString)object)._key);
      }
      else if (object instanceof String)
      {
         return _key.equals(object);
      }
      else
      {
         return _key.equals(object.toString()) || _text.equals(object.toString());
      }
   }

   /* (non-Javadoc)
    * @see java.lang.Object#hashCode()
    */
   @Override
   public int hashCode()
   {
      return _key.hashCode();
   }

   /* (non-Javadoc)
    * @see java.lang.Comparable#compareTo(java.lang.Object)
    */
   @Override
   public int compareTo(Object object)
   {
      if (object instanceof LocalizedString)
      {
         return _key.compareTo(((LocalizedString)object)._key);
      }
      else if (object instanceof String)
      {
         return _key.compareTo((String) object);
      }
      else
      {
         return _text.compareTo(object.toString());
      }
   }
}
