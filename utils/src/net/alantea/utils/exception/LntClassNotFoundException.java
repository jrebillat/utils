/****************************************************************************
 * 
 * Innovation Technologies Support, Services and Solutions
 * 
 * Copyright (c) Airbus Defence and Space 2015
 */
//  
/****************************************************************************************************/
package net.alantea.utils.exception;

import net.alantea.utils.MultiMessages;

/**
 * *********************************.
 */

/************************************/
/**
 * AddOn not found Exception.
 * @author Alantea
 *
 */
public class LntClassNotFoundException extends LntException
{
   
   /** The Constant serialVersionUID. */
   private static final long serialVersionUID = 1L;

   /**
    * Constructor.
    * @param name of the searched addon.
    */
   public LntClassNotFoundException(String name)
   {
      super(MultiMessages.get("GnerikClassNotFoundException.error", name, ""));
   }

   /**
    * Constructor.
    * @param name of the searched addon.
    * @param cause of the exception
    */
   public LntClassNotFoundException(String name, Throwable cause)
   {
      super(MultiMessages.get("GnerikClassNotFoundException.error", name, cause.getMessage()), cause);
   }
}
