/****************************************************************************
 * 
 * Innovation Technologies Support, Services and Solutions
 * 
 * Copyright (c) Airbus Defence and Space 2015
 */
//  
/****************************************************************************************************/
package net.alantea.utils.exception;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.alantea.utils.MultiMessages;

/**
 * *********************************.
 */
/**
 * general Exception that logs information.
 * @author Alantea
 *
 */
public class LntException extends Exception
{
   
   /**
    * The Enum LEVEL.
    */
   public static enum LEVEL
   {
      
      /** The log error. */
      LOG_ERROR,
      
      /** The log debug. */
      LOG_DEBUG,
      
      /** The log warn. */
      LOG_WARN,
      
      /** The log info. */
      LOG_INFO
   }
   
   /** The logger. */
   private static Logger logger = LoggerFactory.getLogger(LntException.class);
   
   /** The exception list. */
   private static List<LntException> exceptionList = new LinkedList<>();

   
   /** The Constant serialVersionUID. */
   private static final long serialVersionUID = 1L;
   
   static
   {
      MultiMessages.addBundle("net.alantea.utils.Utils");
   }
   
   /** The level. */
   private LEVEL level;

   /**
    * Constructor.
    * @param text to show.
    */
   public LntException(String text)
   {
      this(text, LEVEL.LOG_WARN);
   }

   /**
    * Constructor.
    * @param text to show.
    * @param cause of the exception
    */
   public LntException(String text, Throwable cause)
   {
      this(text, LEVEL.LOG_WARN, cause);
   }

   /**
    * Constructor.
    *
    * @param text to show.
    * @param level the level
    */
   public LntException(String text, LEVEL level)
   {
      super(generateMessageText(text));
      initialize(text, level);
   }

   /**
    * Constructor.
    *
    * @param text to show.
    * @param level the level
    * @param cause the cause
    */
   public LntException(String text, LEVEL level, Throwable cause)
   {
      super(generateMessageText(text), cause);
      initialize(text, level);
   }

   /**
    * Initialize.
    *
    * @param text the text
    * @param level the level
    */
   private void initialize(String text, LEVEL level)
   {
      switch(level)
      {
         case LOG_ERROR :
            logger.error(text);
            break;

         case LOG_DEBUG :
            logger.debug(text);
            break;

         case LOG_WARN :
            logger.warn(text);
            break;

         case LOG_INFO :
            logger.info(text);
            break;
            
         default : 
            logger.warn(text);
      }
      this.level = level;
      addToList(this);
   }
   
   /**
    * Peek exceptions.
    *
    * @return a copy of the list
    */
   public static List<LntException> peekExceptions()
   {
      return new LinkedList<LntException>(exceptionList);
   }
   
   /**
    * Pop exceptions. Clear the list.
    *
    * @return the list
    */
   public static List<LntException> popExceptions()
   {
      List<LntException> list = exceptionList;
      exceptionList = new LinkedList<>();
      return list;
   }
   
   /**
    * Peek first exception.
    *
    * @return the first LntException or null.
    */
   public static LntException peek()
   {
      if (exceptionList.size() == 0)
      {
         return null;
      }
      return exceptionList.get(0);
   }
   
   /**
    * Pop first exception. Clear it from the list.
    *
    * @return the first LntException or null.
    */
   public static LntException pop()
   {
      if (exceptionList.size() == 0)
      {
         return null;
      }
      LntException ret = exceptionList.get(0);
      exceptionList.remove(0);
      return ret;
   }
   
   /**
    * Adds the given element to the list.
    *
    * @param exception the exception to be added
    */
   private static void addToList(LntException exception)
   {
      if (exceptionList.size() >= 10)
      {
         exceptionList.remove(0);
      }
      exceptionList.add(exception);
   }
   
   /**
    * Gets the log level.
    *
    * @return the level
    */
   public LEVEL getLevel()
   {
      return level;
   }
   
   /**
    * Generate the exception text using messaging.
    *
    * @param key the key
    * @return the text
    */
   private static String generateMessageText(String key)
   {
      try
      {
         return MultiMessages.get(key);
      }
      catch (Exception e)
      {
         return key;
      }
   }
}
