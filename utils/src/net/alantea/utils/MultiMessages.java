package net.alantea.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

import net.alantea.utils.exception.LntException;

/**
 * Class to manage messages from various properties files.
 * 
 * @author Alantea
 * 
 */
public class MultiMessages
{
  
  /** The bundles. */
  private static final LinkedList<ResourceBundle> bundles = new LinkedList<>();
  
  /** The first bundle. */
  private static ResourceBundle firstBundle;
  
  /** The default bundle. */
  private static ResourceBundle defaultBundle;

  /** The locale. */
  private static final Locale locale = Locale.getDefault();

  private static final List<MessageProvider> providers = new LinkedList<>();
  
  private static final Map<String, String> applicativeMap = new HashMap<>();

  /** Private singleton constructor. */
  private MultiMessages()
  {
  }
  
  /**
   * Gets the locale.
   *
   * @return the locale
   */
  public static Locale getLocale()
  {
     return locale;
  }
  
  public static void addApplicativeKey(String key, String value)
  {
     applicativeMap.put(key, value);
  }
  
  public static void removeApplicativeKey(String key)
  {
     applicativeMap.remove(key);
  }

  /**
   * Adds the associated bundle.
   *
   * @param object the object
   * @return true, if successful
   */
  public static boolean addAssociatedBundle(Object object)
  {
    ResourceBundle bundle = null;
    String name = null;
    try
    {
       if (object instanceof Class<?>)
       {
          name = ((Class<?>) object).getName();
       }
       else
       {
          name = object.getClass().getName();
       }
      bundle = manageBundle(name, true);
    }
    catch (MissingResourceException e)
    {
      new LntException("Missing resource file : " + name + ".properties", e);
    }
    return bundle != null;
  }

  /**
   * Adds the bundle.
   *
   * @param name the bundle path and name
   * @return true, if successful
   */
  public static boolean addBundle(String name)
  {
    ResourceBundle bundle = null;

    try
    {
      bundle = manageBundle(name, true);
    }
    catch (MissingResourceException e)
    {
       new LntException("Missing resource file : " + name + ".properties", e);
    }
    return bundle != null;
  }

  /**
   * Adds the provider.
   *
   * @param provider the provider
   */
  public static void addProvider(MessageProvider provider)
  {
     providers.add(provider);
  }
  
  /**
   * Sets the first bundle.
   *
   * @param name the new first bundle
   */
  public static void setFirstBundle(String name)
  {
     if (firstBundle != null)
     {
        bundles.addFirst(firstBundle);
     }
     firstBundle = manageBundle(name, false);
  }
  
  /**
   * Sets the default bundle.
   *
   * @param name the new default bundle
   */
  public static void setDefaultBundle(String name)
  {
     defaultBundle = manageBundle(name, false);
  }

  /**
   * Gets the bundle.
   *
   * @param object the object
   * @return the bundle
   * @throws MissingResourceException the missing resource exception
   */
  public static ResourceBundle getBundle(Object object)
      throws MissingResourceException
  {
     String name = object.getClass().getName();
    return manageBundle(name, false);
  }

  /**
   * Gets the bundle.
   *
   * @param object the object
   * @return the bundle
   * @throws MissingResourceException the missing resource exception
   */
  public static ResourceBundle getBundle(Class<?> object)
      throws MissingResourceException
  {
     String name = object.getName();
    return manageBundle(name, false);
  }

  /**
   * Gets the bundle.
   *
   * @param path the path
   * @return the bundle
   * @throws MissingResourceException the missing resource exception
   */
  public static ResourceBundle getBundle(String path)
      throws MissingResourceException
  {
    return manageBundle(path, false);
  }

  /**
   * Manage bundle.
   *
   * @param path the path
   * @param storeIt the store it
   * @return the resource bundle
   * @throws MissingResourceException the missing resource exception
   */
  private static ResourceBundle manageBundle(String path, boolean storeIt)
      throws MissingResourceException
  {
    ResourceBundle bundle = null;

    bundle = ResourceBundle.getBundle(path, locale, new UTF8Control());
    if ((storeIt) && (!bundles.contains(bundle)))
    {
       bundles.addFirst(bundle);
    }
    return bundle;
  }

  /**
   * Get an integer from its key.
   *
   * @param key          to search
   * @return the message value or 0.
   */
  public static int getAsInt(String key)
  {
     int ret = 0;
     String txt = get(key);
     try
     {
        ret = Integer.parseInt(txt);
     }
     catch (NumberFormatException ex)
     {
        // nothing
     }
     return ret;
  }

  /**
   * Get a double from its key.
   *
   * @param key          to search
   * @return the message value or 0.0
   */
  public static double getAsDouble(String key)
  {
     double ret = 0;
     String txt = get(key);
     try
     {
        ret = Double.parseDouble(txt);
     }
     catch (NumberFormatException ex)
     {
        // nothing
     }
     return ret;
  }

  /**
   * Get a message from its key.
   *
   * @param key          to search
   * @param args the args
   * @return the message value or the key if nothing is found
   */
  public static String get(String key, Object... args)
  {
    String ret = key;
    
    // Search in applicative values
    ret = applicativeMap.get(key);

    // Search in first bundle
    if ((ret == null) || (ret.equals(key)))
    {
       ret = searchInBundle(key, ret, firstBundle);
    }

    // Search in providers
    if ((ret == null) || (ret.equals(key)))
    {
       ret = searchInProviders(key, args);
    }

    // search in registered bundles
    if ((ret == null) || (ret.equals(key)))
    {
       for (ResourceBundle bundle : bundles)
       {
          ret = searchInBundle(key, ret, bundle);
       }
    }

    if ((ret == null) || (ret.equals(key)))
    {
       ret = searchInBundle(key, ret, defaultBundle);
    }

      // replace arguments
      if (ret != null)
      {
         for (int i = 0; i < args.length; i++)
         {
            String arg = args[i].toString();
            if (arg == null)
            {
               arg = "'null'";
            }
            ret = ret.replaceAll("\\[" + (i + 1) + "\\]", arg);
         }
      }
      
      if (ret == null)
      {
         ret = key;
      }
      return ret;
  }

  private static String searchInProviders(String key, Object[] args)
  {
     String ret = null;
     for (MessageProvider provider : providers)
     {
        ret = provider.get(key, locale, args);
     }
     return ret;
  }

/**
   * Search in bundle.
   *
   * @param key the key
   * @param alreadyFound the already found
   * @param bundle the bundle
   * @return the string
   */
  private static String searchInBundle(String key, String alreadyFound, ResourceBundle bundle)
  {
     String ret = alreadyFound;
     try
     {
        if (bundle != null)
        {
           if ((alreadyFound == null) || (alreadyFound.equals(key)))
           {
              ret = bundle.getString(key);
           }
        }
     }
     catch (MissingResourceException e)
     {
        // nothing
     }
     return ret;
  }
}

  /**
   * Control for getting bundle from an UTF-8 file. This work is got from the
 * article "How to use UTF-8 in resource properties with ResourceBundle" found
 * on internet at :
 * http://stackoverflow.com/questions/4659929/how-to-use-utf-8-in
 * -resource-properties-with-resourcebundle
 *
 */
class UTF8Control extends Control
{
  
  /**
   * Bundle creation.
   *
   * @param baseName the base name
   * @param locale the locale
   * @param format the format
   * @param loader the loader
   * @param reload the reload
   * @return the resource bundle
   * @throws IllegalAccessException           when raised.
   * @throws InstantiationException           when raised.
   * @throws IOException           when raised.
   */
  public ResourceBundle newBundle(String baseName, Locale locale,
      String format, ClassLoader loader, boolean reload)
      throws IllegalAccessException, InstantiationException, IOException
  {
    // The below is a copy of the default implementation.
    String bundleName = toBundleName(baseName, locale);
    String resourceName = "/" + toResourceName(bundleName, "properties");
    ResourceBundle bundle = null;
    InputStream stream = null;

    stream = MultiMessages.class.getResourceAsStream(resourceName);

    if (stream == null)
    {
      resourceName = "/" + toResourceName(baseName, "properties");
      stream = MultiMessages.class.getResourceAsStream(resourceName);

    }

    if (stream != null)
    {
      try
      {
        // Only this line is changed to make it to read properties files as
        // UTF-8.
        bundle = new PropertyResourceBundle(new InputStreamReader(stream,
            "UTF-8"));
      }
      finally
      {
        stream.close();
      }
    }
    return bundle;
  }
}
