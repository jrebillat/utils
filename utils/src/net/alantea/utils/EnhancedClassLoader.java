package net.alantea.utils;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * The Class StorexClassHandler.
 */
public class EnhancedClassLoader extends AppendableUrlClassLoader 
{
   /** The loaders map. */
   private static Map<String, Class<?>> loadersMap = new HashMap<>();
   
   /**
    * Instantiates a new storex class handler.
    *
    * @param parent the parent
    */
   public EnhancedClassLoader(ClassLoader parent)
   {
      super(parent, new URL[0]);
   }

   public EnhancedClassLoader(Class<?> reference)
   {
      super(reference.getClassLoader(), new URL[0]);
   }
   
   /**
    * Map class.
    *
    * @param classtag the classtag
    * @param associatedClass the associated class
    */
   public static void mapClass(String classtag, Class<?> associatedClass)
   {
      loadersMap.put(classtag, associatedClass);
   }
   
   /**
    * Gets the class from map.
    *
    * @param key the key
    * @return the class from map
    */
   private static Class<?> getClassFromMap(String key)
   {
      return loadersMap.get(key);
   }

   /**
    * Adds the URL.
    *
    * @param url the url
    */
   /* (non-Javadoc)
    * @see java.net.URLClassLoader#addURL(java.net.URL)
    */
   public void addURL(URL url)
   {
       super.addURL(url);
   }

   /**
    * Adds the jar file.
    *
    * @param url the url
    */
   public void addClassesFile(URL url)
   {
       addURL(url);
   }

   /**
    * Adds the jar file.
    *
    * @param file the file
    */
   public void addClassesFile(File file)
   {
      try
      {
         addURL(file.toURI().toURL());
      }
      catch (MalformedURLException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   /**
    * Load class.
    *
    * @param name the name
    * @return the class
    * @throws ClassNotFoundException the class not found exception
    */
   @Override
   public Class<?> loadClass(String name) throws ClassNotFoundException
   {
      Class<?> ret = getClassFromMap(name);
      if (ret == null)
      {
         ret = loadClass(name, false);
      }
      return ret;
   }
}
