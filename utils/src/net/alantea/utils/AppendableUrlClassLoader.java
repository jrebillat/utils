package net.alantea.utils;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.LinkedList;
import java.util.List;

import net.alantea.utils.exception.LntException;

/**
 * The Class AppendableURLClassLoader.
 */
public class AppendableUrlClassLoader extends URLClassLoader
{
   
   private List<String> paths = new LinkedList<>();
   
   /**
    * Instantiates a new appendable URL class loader.
    *
    * @param urls the urls
    */
   public AppendableUrlClassLoader(URL... urls)
   {
      super(urls);
      for (URL url : urls)
      {
         paths.add(url.toString());
      }
   }
   
   /**
    * Instantiates a new appendable URL class loader.
    *
    * @param parent the parent
    * @param urls the urls
    */
   public AppendableUrlClassLoader(ClassLoader parent, URL... urls)
   {
      super(urls, parent);
      for (URL url : urls)
      {
         paths.add(url.toString());
      }
   }

   /**
    * Append URL.
    *
    * @param url the url
    */
   public void appendUrl(URL url)
   {
      if (!paths.contains(url.toString()))
      {
         this.addURL(url);
         paths.add(url.toString());
      }
   }

   /**
    * Adds the source, directory or jar file.
    *
    * @param file the file
    */
   public void appendSource(File file)
   {
      if(file.exists())
      {
         try
         {
            URL url = file.toURI().toURL();
            appendUrl(url);
         }
         catch (MalformedURLException e)
         {
            new LntException("Unable to load source : " + file.getAbsolutePath(), e);
         }
      }
   }

   /**
    * Adds the links.
    *
    * @param links the links
    */
   public void appendPaths(String... links)
   {
      for (String path : links)
      {
         try
         {
            if (!path.contains("://"))
            {
               if (path.endsWith(".jar"))
               {
                  appendSource(new File(path));
               }
               else if (new File(path).isDirectory())
               {
                  appendDirectoryContent(path);
               }
               else
               {
                  new LntException("Unable to load unknown path type : " + path);
               }
            }
            else
            {
               URL url = new URL(path);
               appendUrl(url);
            }
         }
         catch (MalformedURLException e)
         {
            new LntException("Unable to load path : " + path, e);
         }
      }
   }

   /**
    * Append directory content.
    *
    * @param path the path
    */
   public void appendDirectoryContent(String path)
   {
      appendDirectoryContent(path, true);
   }

   /**
    * Append directory content, either all jar files or as class source.
    *
    * @param path the path
    * @param useAsClassSource the use as class source
    */
   public void appendDirectoryContent(String path, boolean useAsClassSource)
   {
      File dir = new File(path);
      if ((dir.exists()) && (dir.isDirectory()))
      {
         if (useAsClassSource)
         {
            appendSource(dir);
         }
         for (String name : dir.list())
         {
            if (name.endsWith(".jar"))
            {
               appendSource(new File(name));
            }
            else if (new File(name).isDirectory())
            {
               appendDirectoryContent(name, false);
            }
         }
      }
      
   }

   public boolean containsPath(String path)
   {
      return paths.contains(path);
   }
}