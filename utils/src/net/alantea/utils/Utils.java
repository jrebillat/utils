package net.alantea.utils;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/************************************/

import net.alantea.utils.exception.LntClassNotFoundException;
import net.alantea.utils.exception.LntException;

/**
 * Utility class.
 *
 * @author Alantea
 */
public final class Utils
{

   /** The profiling activated. */
   private static boolean profilingActivated;
   
   /** The appendable URL class loader. */
   private static AppendableUrlClassLoader appendableURLClassLoader = null;

   /**
    * Instantiates a new utils.
    */
   private Utils()
   {

   }

   /**
    * Get class knowing the class name.
    *
    * @param <T>       the generic type
    * @param className name of the class.
    * @return the class.
    * @throws LntException if something went wrong.
    */
   @SuppressWarnings("unchecked")
   static <T> Class<? extends T> getClass(String className) throws LntException
   {
      // search class
      Class<?> cl = null;
      try
      {
         cl = Class.forName(className);
      }
      catch (ClassNotFoundException | SecurityException | IllegalArgumentException e)
      {
         throw new LntClassNotFoundException(className, e);
      }
      return (Class<? extends T>) cl;
   }

   /**
    * Get class constructor.
    *
    * @param <T> the generic type
    * @param cl  class to analyse.
    * @return the class constructor.
    * @throws LntException if something went wrong.
    */
   static <T> Constructor<? extends T> getConstructor(Class<? extends T> cl) throws LntException
   {
      // search constructor
      Class<?>[] argTypes = new Class<?>[0];
      Constructor<? extends T> constructor;
      try
      {
         constructor = (Constructor<? extends T>) cl.getDeclaredConstructor(argTypes);
      }
      catch (NoSuchMethodException e)
      {
         throw new LntClassNotFoundException(cl.getName(), e);
      }
      catch (SecurityException e)
      {
         throw new LntClassNotFoundException(cl.getName(), e);
      }
      return constructor;
   }

   /**
    * Get class instance.
    *
    * @param <T>         the generic type
    * @param className   the class name
    * @param constructor of class to instantiate.
    * @param args        the arguments
    * @return the instance.
    * @throws LntException if something went wrong.
    */
   static <T> T getInstance(String className, Constructor<? extends T> constructor, Object... args)
           throws LntException
   {
      T ret = null;
      try
      {
         if (!constructor.isAccessible())
         {
            constructor.setAccessible(true);
         }
         ret = constructor.newInstance(args);
      }
      catch (InstantiationException | IllegalAccessException
              | IllegalArgumentException | InvocationTargetException e)
      {
         throw new LntClassNotFoundException(className, e);
      }
      return ret;
   }

   /**
    * Activate profiling.
    *
    * @param flag the flag
    */
   public static void activateProfiling(boolean flag)
   {
      profilingActivated = flag;
   }

   /**
    * Profiling.
    */
   public static void profiling()
   {
      if (profilingActivated)
      {
         StackTraceElement[] list = Thread.currentThread().getStackTrace();
         Date date = new Date();
         System.out.println(date.toString() + " (" + date.getTime() + ") - " + list[2].toString());
      }
   }

   /**
    * Append jar class loader.
    *
    * @param path the path
    * @return the appendable url class loader
    */
   public static AppendableUrlClassLoader appendJarClassLoader(String path)
   {
      File file = new File(path);
      if (file.exists())
      {
         try
         {
            if (appendableURLClassLoader == null)
            {
               appendableURLClassLoader = new AppendableUrlClassLoader(file.toURI().toURL());
            }
            else
            {
               appendableURLClassLoader.appendUrl(file.toURI().toURL());
            }
         }
         catch (MalformedURLException e)
         {
            System.out.println("Error : Non-accessible jar file : " + path);
         }
      }
      return appendableURLClassLoader;
   }
   
   /**
    * Parses the annotated arguments.
    *
    * @param <X> the generic type
    * @param cl the cl
    * @param args the args
    * @return the x
    */
   public static <X> X parseArguments(Class<X> cl, String[] args)
   {
      if (cl == null)
      {
         return null;
      }
      
      try
      {
         Constructor<X> constructor = cl.getConstructor();
         constructor.setAccessible(true);
         X arguments = constructor.newInstance();
         parseArguments(arguments, args);
         return arguments;
      }
      catch (Exception e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      return null;
   }
   
   /**
    * Parses the annotated arguments.
    *
    * @param target the target
    * @param args the arguments
    * @return true, if successful
    */
   public static boolean securedParseArguments(Object target, String[] args)
   {
       try
       {
		  parseArguments(target, args);
		  return true;
	   }
       catch (IllegalArgumentException | IllegalAccessException | ParseException e)
       {
		  // Silently ignore it...
    	   return false;
	   }
   }
   
   /**
    * Parses the annotated arguments.
    *
    * @param target the target
    * @param args the arguments
    * @throws IllegalArgumentException the illegal argument exception
    * @throws IllegalAccessException the illegal access exception
    * @throws ParseException the parse exception
    */
   public static void parseArguments(Object target, String[] args) throws IllegalArgumentException, IllegalAccessException, ParseException
   {
      if (target == null)
      {
         return;
      }
          
         Field[] fields = target.getClass().getDeclaredFields();
         if (fields.length == 0)
         {
            return;
         }

         List<Field> usableFields = new LinkedList<>();
         List<String> usableNames = new LinkedList<>();
         Options options = new Options();
         for (Field field : fields)
         {
            ArgumentName annotation = field.getAnnotation(ArgumentName.class);
            ArgumentShortName shortAnnotation = field.getAnnotation(ArgumentShortName.class);
            ArgumentDescription descriptionAnnotation = field.getAnnotation(ArgumentDescription.class);
            if ((annotation == null) && (shortAnnotation == null))
            {
               continue;
            }
            field.setAccessible(true);
            String optionName = annotation.value();
            if (optionName.length() == 0)
            {
               optionName = field.getName();
            }
            String shortOptionName = "";
            if (shortAnnotation != null)
            {
               shortOptionName = shortAnnotation.value();
            }
            String optionDescription = optionName;
            if ((descriptionAnnotation != null))
            {
               optionDescription = descriptionAnnotation.value();
            }
            boolean withParameterValue = false;
            boolean usableField = false;
            if ((field.getType().equals(Boolean.class)) || (field.getType().equals(Boolean.TYPE)))
            {
               usableField = true;
               withParameterValue = false;
            }
            else if (field.getType().equals(String.class))
            {
               usableField = true;
               withParameterValue = true;
            }
            
            if (usableField)
            {
               usableFields.add(field);
               usableNames.add(optionName);
               if ((shortOptionName != null) && (shortOptionName.length() != 0))
               {
                   options.addOption(shortOptionName, optionName, withParameterValue, optionDescription);
               }
               else
               {
                  options.addOption(optionName, withParameterValue, optionName);
               }
            }
         }

         CommandLineParser parser = new DefaultParser();
         CommandLine cmd = parser.parse(options, args);
         for (int i = 0; i < usableFields.size(); i++)
         {
            Field field = usableFields.get(i);
            String optionName = usableNames.get(i);
            if ((field.getType().equals(Boolean.class)) || (field.getType().equals(Boolean.TYPE)))
            {
               field.setBoolean(target, cmd.hasOption(optionName));
            }
            else if (field.getType().equals(String.class))
            {
               String value = cmd.getOptionValue(optionName);
               if (value != null)
               {
                  field.set(target, value);
               }
            }
         }
   }
}
