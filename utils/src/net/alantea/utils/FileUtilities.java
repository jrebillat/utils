package net.alantea.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;

/**
 * The Files utilities.
 */
public final class FileUtilities
{
   
   /**
    * Instantiates a new files.
    */
   private FileUtilities() {}
   
   /**
    * Copy.
    *
    * @param source the source
    * @param target the target
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public static void copy(InputStream source, OutputStream target) throws IOException
   {
      byte[] buf = new byte[8192];
      int length;
      while ((length = source.read(buf)) > 0)
      {
          target.write(buf, 0, length);
      }
  }

   /**
    * Creates a temporary directory that will be automatically deleted on exit.
    *
    * @param key the key
    * @return the file
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public static File createTemporaryDirectory(String key) throws IOException
   {
      File file = Files.createTempDirectory(key).toFile();
      markForDeletionOnExit(file);
      return file;
   }
   
   /**
    * Mark for deletion on exit.
    * Wipes all the content, recursively, for a directory.
    * We sometimes need to remove directories "manually" at shutdown, as they only get cleared automatically if they are empty.
    *
    * @param file the file
    */
   public static void markForDeletionOnExit(File file)
   {
     // Need to remove directories "manually" at shutdown, as they only get cleared automatically if they are empty.
     Runtime.getRuntime().addShutdownHook(new Thread()
     {
        @Override
        public void run()
        {
           if (file.isDirectory())
           {
              deleteDirectory(file);
           }
           else if (file.isFile())
           {
              file.delete();
           }
        }
     });
  }
   
  /**
   * Delete directory and all its content, recursively.
   *
   * @param file the file
   */
  public static void deleteDirectory(File file)
   {
      if ((file != null) && (file.exists()))
      {
         if (file.isDirectory())
         {
            for (File subfile : file.listFiles())
            {
               deleteDirectory(subfile);
            }
         }
         file.delete();
      }
   }


  /**
   * Read all lines from file path.
   *
   * @param filepath the file path
   * @return the execution report
   */
  public static List<String> readAllLines(String filepath)
  {
     return readAllLines(new File(filepath));
  }

  /**
   * Read all lines from file.
   *
   * @param file the file
   * @return the execution report
   */
  public static List<String> readAllLines(File file)
  {
     List<String> ret = null;
     if (file.exists())
     {
        try
        {
           FileReader reader = new FileReader(file);
           ret = readAllLines(reader);
           reader.close();
        }
        catch (IOException e)
        {
           e.printStackTrace();
        }
     }
     return ret;
  }

  /**
   * Read all lines from input stream.
   *
   * @param stream the stream
   * @return the execution report
   */
  public static List<String> readAllLines(InputStream stream)
  {
     Reader reader = new InputStreamReader(stream);
     List<String> ret = readAllLines(reader);
     try
     {
        reader.close();
     }
     catch (IOException e)
     {
        e.printStackTrace();
     }
     return ret;
  }
  
  /**
   * Read all lines from any type of Reader.
   *
   * @param reader the reader
   * @return the list
   */
  public static List<String> readAllLines(Reader reader)
  {
     List<String> ret = new LinkedList<>();
     try (BufferedReader bufreader = new BufferedReader(reader))
     {
        String line;
        while ((line =  bufreader.readLine()) != null)
        {
           ret.add(line);
        }
     } catch (IOException e) {
        e.printStackTrace();
    }
    return ret;
  }
}
