package net.alantea.utils;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public interface StreamLister<T>
{
   public static <T> List<T> getByComparison(List<T> input, Comparator<? super T> comparator)
   {
      return input.stream().sorted(comparator).collect(Collectors.toList());
   }

   public static <T>  List<T> getByFilter(List<T> input, Predicate<? super T> predicate)
   {
      return input.stream().filter(predicate).collect(Collectors.toList());
   }

   public static <T>  List<T> getByFilter(List<T> input, Predicate<? super T> predicate,
         Comparator<? super T> comparator)
   {
      return input.stream().filter(predicate).sorted(comparator).collect(Collectors.toList());
   }
   
   public default List<T> getElementsByComparison(Comparator<? super T> comparator)
   {
      return getByComparison(getElements(), comparator);
   }

   public default  List<T> getElementsByFilter(Predicate<? super T> predicate)
   {
      return getByFilter(getElements(), predicate);
   }

   public default  List<T> getElementsByFilter(Predicate<? super T> predicate,
         Comparator<? super T> comparator)
   {
      return getByFilter(getElements(), predicate, comparator);
   }
   
   public List<T> getElements();

}
