package net.alantea.utils.test.streamlister;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;

import net.alantea.utils.StreamLister;

/**
 * The Class StreamListerTest.
 */
public class StreamListerTest
{
   
   /** The elt 01. */
   private static Element elt01 = new Element(8, 1, "One");
   
   /** The elt 02. */
   private static Element elt02 = new Element(7, 2, "Two");
   
   /** The elt 03. */
   private static Element elt03 = new Element(6, 3, "Three");
   
   /** The elt 04. */
   private static Element elt04 = new Element(5, 4, "Four");
   
   /** The elt 11. */
   private static Element elt11 = new Element(4, 1, "ONE");
   
   /** The elt 12. */
   private static Element elt12 = new Element(3, 2, "TWO");
   
   /** The elt 13. */
   private static Element elt13 = new Element(2, 3, "THREE");
   
   /** The elt 14. */
   private static Element elt14 = new Element(1, 4, "FOUR");
   
   /**
    * Simple stream lister test.
    */
   @Test
   public void simpleStreamListerTest()
   {
      TestStreamer stream = new TestStreamer(elt01, elt02, elt03, elt04, elt11, elt12, elt13, elt14);
      Assertions.assertNotNull(stream);
      
      List<Element> elts = stream.getElements();
      Assertions.assertNotNull(elts);
      Assertions.assertEquals(8, elts.size());
      
      List<Element> filtered = stream.getElementsByFilter((elt) ->  (elt.getIntInfo() == 1));
      Assertions.assertNotNull(filtered);
      Assertions.assertEquals(2, filtered.size());
      Assertions.assertTrue((filtered.contains(elt01)) && (filtered.contains(elt11)));
      
      List<Element> ordered = stream.getElementsByComparison((a, b) ->  (a.getOrder() - b.getOrder()));
      Assertions.assertNotNull(ordered);
      Assertions.assertEquals(8, ordered.size());
      Assertions.assertEquals(elt14, ordered.get(0));
      Assertions.assertEquals(elt13, ordered.get(1));
      Assertions.assertEquals(elt12, ordered.get(2));
      Assertions.assertEquals(elt11, ordered.get(3));
      Assertions.assertEquals(elt04, ordered.get(4));
      Assertions.assertEquals(elt03, ordered.get(5));
      Assertions.assertEquals(elt02, ordered.get(6));
      Assertions.assertEquals(elt01, ordered.get(7));
      
      List<Element> mix = stream.getElementsByFilter((elt) ->  (elt.getIntInfo() == 2),
            (a, b) ->  (a.getOrder() - b.getOrder()));
      Assertions.assertNotNull(mix);
      Assertions.assertEquals(2, mix.size());
      Assertions.assertEquals(elt12, mix.get(0));
      Assertions.assertEquals(elt02, mix.get(1));
   }

   /**
    * Static stream lister methods test.
    */
   @Test
   public void staticStreamListerMethodsTest()
   {
      List<Element> list = Arrays.asList(elt01, elt02, elt03, elt04, elt11, elt12, elt13, elt14);
      
      List<Element> filtered = StreamLister.getByFilter(list, (elt) ->  (elt.getIntInfo() == 1));
      Assertions.assertNotNull(filtered);
      Assertions.assertEquals(2, filtered.size());
      Assertions.assertTrue((filtered.contains(elt01)) && (filtered.contains(elt11)));
      
      List<Element> ordered = StreamLister.getByComparison(list, (a, b) -> a.getOrder() - b.getOrder());
      Assertions.assertNotNull(ordered);
      Assertions.assertEquals(8, ordered.size());
      Assertions.assertEquals(elt14, ordered.get(0));
      Assertions.assertEquals(elt13, ordered.get(1));
      Assertions.assertEquals(elt12, ordered.get(2));
      Assertions.assertEquals(elt11, ordered.get(3));
      Assertions.assertEquals(elt04, ordered.get(4));
      Assertions.assertEquals(elt03, ordered.get(5));
      Assertions.assertEquals(elt02, ordered.get(6));
      Assertions.assertEquals(elt01, ordered.get(7));
      
      List<Element> mix = StreamLister.getByFilter(list, (elt) ->  (elt.getIntInfo() == 2),
            (a, b) ->  (a.getOrder() - b.getOrder()));
      Assertions.assertNotNull(mix);
      Assertions.assertEquals(2, mix.size());
      Assertions.assertEquals(elt12, mix.get(0));
      Assertions.assertEquals(elt02, mix.get(1));
   }
   
   /**
    * The Class Element.
    */
   private static class Element
   {
      
      /** The order. */
      private int order = 0;
      
      /** The int info. */
      private int intInfo = 0;
      
      /** The string info. */
      //private String stringInfo = "";
      
      /**
       * Instantiates a new element.
       *
       * @param order the order
       * @param intInfo the int info
       * @param stringInfo the string info
       */
      public Element(int order, int intInfo, String stringInfo)
      {
         super();
         this.order = order;
         this.intInfo = intInfo;
         //this.stringInfo = stringInfo;
      }

      /**
       * Gets the order.
       *
       * @return the order
       */
      public int getOrder()
      {
         return order;
      }

      /**
       * Gets the int info.
       *
       * @return the int info
       */
      public int getIntInfo()
      {
         return intInfo;
      }

//      /**
//       * Gets the string info.
//       *
//       * @return the string info
//       */
//      public String getStringInfo()
//      {
//         return stringInfo;
//      }
   }
   
   /**
    * The Class TestStreamer.
    */
   private class TestStreamer implements StreamLister<Element>
   {
      
      /** The list. */
      private List<Element> list;
      
      /**
       * Instantiates a new test streamer.
       *
       * @param elements the elements
       */
      public TestStreamer(Element...elements)
      {
         list = new LinkedList<>();
         list.addAll(Arrays.asList(elements));
      }

      /**
       * Gets the elements.
       *
       * @return the elements
       */
      @Override
      public List<Element> getElements()
      {
         return list;
      }
   }
}
