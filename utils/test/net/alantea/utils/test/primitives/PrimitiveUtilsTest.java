package net.alantea.utils.test.primitives;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.utils.PrimitiveUtils;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PrimitiveUtilsTest
{
   @Test
   @Order(1)
   void testPrimitive()
   {
      boolean res = PrimitiveUtils.testPrimitive(Integer.TYPE);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.testPrimitive(Double.TYPE);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.testPrimitive(Float.TYPE);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.testPrimitive(Long.TYPE);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.testPrimitive(Boolean.TYPE);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.testPrimitive(Double.class);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.testPrimitive(Float.class);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.testPrimitive(Long.class);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.testPrimitive(Boolean.class);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.testPrimitive(String.class);
      Assertions.assertFalse(res);

      res = PrimitiveUtils.testPrimitive(null);
      Assertions.assertFalse(res);
   }

   @Test
   @Order(10)
   void createReserved()
   {
      Object res = PrimitiveUtils.createReserved("int");
      Assertions.assertTrue(res instanceof Integer);

      res = PrimitiveUtils.createReserved("double");
      Assertions.assertTrue(res instanceof Double);

      res = PrimitiveUtils.createReserved("long");
      Assertions.assertTrue(res instanceof Long);

      res = PrimitiveUtils.createReserved("float");
      Assertions.assertTrue(res instanceof Float);

      res = PrimitiveUtils.createReserved("boolean");
      Assertions.assertTrue(res instanceof Boolean);

      res = PrimitiveUtils.createReserved("byte");
      Assertions.assertTrue(res instanceof Byte);

      res = PrimitiveUtils.createReserved("short");
      Assertions.assertTrue(res instanceof Short);

      res = PrimitiveUtils.createReserved("String");
      Assertions.assertTrue(res instanceof String);

      res = PrimitiveUtils.createReserved("string");
      Assertions.assertTrue(res instanceof String);

      res = PrimitiveUtils.createReserved("Something");
      Assertions.assertNull(res);
   }

   @Test
   @Order(11)
   void verifyNotReserved()
   {
      boolean res = PrimitiveUtils.verifyNotReserved("int");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReserved("double");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReserved("long");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReserved("float");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReserved("boolean");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReserved("byte");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReserved("short");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReserved("String");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReserved("string");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReserved("Something");
      Assertions.assertTrue(res);
   }

   @Test
   @Order(12)
   void verifyNotReservedContainer()
   {
      boolean res = PrimitiveUtils.verifyNotReservedContainer("Integer");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReservedContainer("Double");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReservedContainer("Long");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReservedContainer("Float");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReservedContainer("Boolean");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReservedContainer("Byte");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReservedContainer("Short");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReservedContainer("String");
      Assertions.assertFalse(res);

      res = PrimitiveUtils.verifyNotReservedContainer("Something");
      Assertions.assertTrue(res);
   }

   @Test
   @Order(4)
   void getSimpleObject()
   {
      Object res = PrimitiveUtils.getSimpleObject(Integer.class, "123");
      Assertions.assertEquals(123, res);

      res = PrimitiveUtils.getSimpleObject(Long.class, "123");
      Assertions.assertEquals(123L, res);

      res = PrimitiveUtils.getSimpleObject(Double.class, "1.23");
      Assertions.assertEquals(1.23D, res);

      res = PrimitiveUtils.getSimpleObject(Float.class, "1.23");
      Assertions.assertEquals(1.23f, res);

      res = PrimitiveUtils.getSimpleObject(Boolean.class, "true");
      Assertions.assertEquals(Boolean.TRUE, res);

      Byte b = 123;
      res = PrimitiveUtils.getSimpleObject(Byte.class, "123");
      Assertions.assertEquals(b, res);

      res = PrimitiveUtils.getSimpleObject(String.class, "Test");
      Assertions.assertEquals("Test", res);
   }

   @Test
   @Order(3)
   void getSimpleObjectDefaultValue()
   {
      Object res = PrimitiveUtils.getSimpleObjectDefaultValue(Integer.class);
      Assertions.assertEquals(0, res);

      res = PrimitiveUtils.getSimpleObjectDefaultValue(Long.class);
      Assertions.assertEquals(0L, res);

      res = PrimitiveUtils.getSimpleObjectDefaultValue(Double.class);
      Assertions.assertEquals(0D, res);

      res = PrimitiveUtils.getSimpleObjectDefaultValue(Float.class);
      Assertions.assertEquals(0f, res);

      res = PrimitiveUtils.getSimpleObjectDefaultValue(Boolean.class);
      Assertions.assertEquals(Boolean.FALSE, res);

      Byte b = 0;
      res = PrimitiveUtils.getSimpleObjectDefaultValue(Byte.class);
      Assertions.assertEquals(b, res);

      res = PrimitiveUtils.getSimpleObjectDefaultValue(String.class);
      Assertions.assertEquals("", res);
   }

   @Test
   @Order(2)
   void comparePrimitive()
   {
      boolean res = PrimitiveUtils.comparePrimitive(Integer.TYPE, Integer.class);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.comparePrimitive(Long.TYPE, Long.class);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.comparePrimitive(Float.TYPE, Float.class);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.comparePrimitive(Double.TYPE, Double.class);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.comparePrimitive(Byte.TYPE, Byte.class);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.comparePrimitive(Short.TYPE, Short.class);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.comparePrimitive(Boolean.TYPE, Boolean.class);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.comparePrimitive(Integer.class, Integer.TYPE);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.comparePrimitive(Long.class, Long.TYPE);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.comparePrimitive(Float.class, Float.TYPE);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.comparePrimitive(Double.class, Double.TYPE);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.comparePrimitive(Byte.class, Byte.TYPE);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.comparePrimitive(Short.class, Short.TYPE);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.comparePrimitive(Boolean.class, Boolean.TYPE);
      Assertions.assertTrue(res);

      res = PrimitiveUtils.comparePrimitive(Integer.class, Double.class);
      Assertions.assertFalse(res);

      res = PrimitiveUtils.comparePrimitive(Integer.class, null);
      Assertions.assertFalse(res);

      res = PrimitiveUtils.comparePrimitive(Integer.class, Byte[].class);
      Assertions.assertFalse(res);
   }
}