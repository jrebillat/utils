package net.alantea.utils.test.primitives;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.utils.StringUtils;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class StringUtilTest
{
   @Test
   @Order(1)
   void testNull()
   {
      String[] res = StringUtils.intelligentSplit(null);
      Assertions.assertEquals(0, res.length);
   }
   @Test
   @Order(2)
   void testEmpty()
   {
      String[] res = StringUtils.intelligentSplit("");
      Assertions.assertEquals(0, res.length);
   }
   
   @Test
   @Order(3)
   void testOne()
   {
      String[] res = StringUtils.intelligentSplit("test");
      Assertions.assertEquals(1, res.length);
      Assertions.assertEquals("test", res[0]);
   }
   
   @Test
   @Order(4)
   void testTwo()
   {
      String[] res = StringUtils.intelligentSplit("test two");
      Assertions.assertEquals(2, res.length);
      Assertions.assertEquals("test", res[0]);
      Assertions.assertEquals("two", res[1]);
   }
   
   @Test
   @Order(5)
   void testThree()
   {
      String[] res = StringUtils.intelligentSplit("test to three");
      Assertions.assertEquals(3, res.length);
      Assertions.assertEquals("test", res[0]);
      Assertions.assertEquals("to", res[1]);
      Assertions.assertEquals("three", res[2]);
   }
   
   @Test
   @Order(6)
   void testWithQuotes1()
   {
      String[] res = StringUtils.intelligentSplit("test \"to\" three");
      Assertions.assertEquals(3, res.length);
      Assertions.assertEquals("test", res[0]);
      Assertions.assertEquals("to", res[1]);
      Assertions.assertEquals("three", res[2]);
   }
   
   @Test
   @Order(7)
   void testWithQuotes2()
   {
      String[] res = StringUtils.intelligentSplit("test \"to\" \"three\"");
      Assertions.assertEquals(3, res.length);
      Assertions.assertEquals("test", res[0]);
      Assertions.assertEquals("to", res[1]);
      Assertions.assertEquals("three", res[2]);
   }
   
   @Test
   @Order(8)
   void testWithQuotes3()
   {
      String[] res = StringUtils.intelligentSplit("test \"to three\"");
      Assertions.assertEquals(2, res.length);
      Assertions.assertEquals("test", res[0]);
      Assertions.assertEquals("to three", res[1]);
   }
   
   @Test
   @Order(9)
   void testWithQuotes4()
   {
      String[] res = StringUtils.intelligentSplit("test \"to three\" and four");
      Assertions.assertEquals(4, res.length);
      Assertions.assertEquals("test", res[0]);
      Assertions.assertEquals("to three", res[1]);
      Assertions.assertEquals("and", res[2]);
      Assertions.assertEquals("four", res[3]);
   }
   
   @Test
   @Order(10)
   void testWithTwoQuotes()
   {
      String[] res = StringUtils.intelligentSplit("test \"to three\" \"and four\"");
      Assertions.assertEquals(3, res.length);
      Assertions.assertEquals("test", res[0]);
      Assertions.assertEquals("to three", res[1]);
      Assertions.assertEquals("and four", res[2]);
   }
   
   @Test
   @Order(11)
   void testWithStartingQuotes()
   {
      String[] res = StringUtils.intelligentSplit("\"test to three\" \"and four\"");
      Assertions.assertEquals(2, res.length);
      Assertions.assertEquals("test to three", res[0]);
      Assertions.assertEquals("and four", res[1]);
   }
   
   @Test
   @Order(12)
   void testWithErroneousQuotes1()
   {
      String[] res = StringUtils.intelligentSplit("test \"to three \"and four\"");
      Assertions.assertEquals(4, res.length);
      Assertions.assertEquals("test", res[0]);
      Assertions.assertEquals("to three ", res[1]);
      Assertions.assertEquals("and", res[2]);
      Assertions.assertEquals("four", res[3]);
   }
   
   @Test
   @Order(13)
   void testWithErroneousQuotes2()
   {
      String[] res = StringUtils.intelligentSplit("test \"to three\" and four\"");
      Assertions.assertEquals(4, res.length);
      Assertions.assertEquals("test", res[0]);
      Assertions.assertEquals("to three", res[1]);
      Assertions.assertEquals("and", res[2]);
      Assertions.assertEquals("four", res[3]);
   }
   
   @Test
   @Order(14)
   void testWithErroneousQuotes3()
   {
      String[] res = StringUtils.intelligentSplit("\"\"");
      Assertions.assertEquals(0, res.length);
   }
   
   @Test
   @Order(15)
   void testWithErroneousQuotes4()
   {
      String[] res = StringUtils.intelligentSplit("\"");
      Assertions.assertEquals(0, res.length);
   }
}
