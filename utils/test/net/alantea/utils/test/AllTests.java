package net.alantea.utils.test;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

import net.alantea.utils.test.arguments.ArgumentsTest;
import net.alantea.utils.test.exception.ExceptionTest;
import net.alantea.utils.test.messages.MessagesTest;
import net.alantea.utils.test.messages.MultiMessagesTest;
import net.alantea.utils.test.streamlister.StreamListerTest;
import net.alantea.utils.test.primitives.PrimitiveUtilsTest;
import net.alantea.utils.test.primitives.StringUtilTest;

@Suite
@SelectClasses({
   ArgumentsTest.class,
   ExceptionTest.class,
   MessagesTest.class,
   MultiMessagesTest.class,
   PrimitiveUtilsTest.class,
   StringUtilTest.class,
   StreamListerTest.class
})
public class AllTests {
}