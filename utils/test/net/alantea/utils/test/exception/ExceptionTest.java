package net.alantea.utils.test.exception;


import java.util.List;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;

import net.alantea.utils.exception.LntException;


public class ExceptionTest
{
   @Test
   public void simpleExceptionTest()
   {
      LntException except = new LntException("Test it");
      Assertions.assertNotNull(except);
      
      LntException.LEVEL level = except.getLevel();
      Assertions.assertEquals(LntException.LEVEL.LOG_WARN, level);
      
      // Dummy clear exceptions
      LntException.popExceptions();
   }
   
   @Test
   public void someExceptionsTest()
   {
      LntException except1 = new LntException("Test it 1");
      Assertions.assertNotNull(except1);
      LntException except2 = new LntException("Test it 2");
      Assertions.assertNotNull(except2);
      LntException except3 = new LntException("Test it 3");
      Assertions.assertNotNull(except3);
      
      List<LntException> list = LntException.popExceptions();
      Assertions.assertNotNull(list);
      Assertions.assertFalse(list.isEmpty());
      Assertions.assertEquals(3, list.size());
      Assertions.assertEquals(except1, list.get(0));
      Assertions.assertEquals(except2, list.get(1));
      Assertions.assertEquals(except3, list.get(2));
   }
   
   @Test
   public void peekExceptionsTest()
   {
      LntException except1 = new LntException("Test it 1");
      Assertions.assertNotNull(except1);

      List<LntException> list1 = LntException.peekExceptions();
      Assertions.assertNotNull(list1);
      Assertions.assertFalse(list1.isEmpty());
      Assertions.assertEquals(1, list1.size());
      Assertions.assertEquals(except1, list1.get(0));
      
      LntException except2 = new LntException("Test it 2");
      Assertions.assertNotNull(except2);

      List<LntException> list2 = LntException.peekExceptions();
      Assertions.assertNotNull(list2);
      Assertions.assertFalse(list2.isEmpty());
      Assertions.assertEquals(2, list2.size());
      Assertions.assertEquals(except1, list2.get(0));
      Assertions.assertEquals(except2, list2.get(1));
      
      LntException except3 = new LntException("Test it 3");
      Assertions.assertNotNull(except3);
      
      List<LntException> list = LntException.popExceptions();
      Assertions.assertNotNull(list);
      Assertions.assertFalse(list.isEmpty());
      Assertions.assertEquals(3, list.size());
      Assertions.assertEquals(except1, list.get(0));
      Assertions.assertEquals(except2, list.get(1));
      Assertions.assertEquals(except3, list.get(2));
   }
}
