package net.alantea.utils.test.messages;

import net.alantea.utils.MessageProvider;

public class TestProvider extends MessageProvider
{
   @Override
   protected String get(String key, Object[] args)
   {
      if ("MyProvider.key".equals(key))
      {
         return "This is my value";
      }
      return null;
   }
}
