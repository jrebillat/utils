package net.alantea.utils.test.messages;


import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * The type Multi messages test.
 */
@TestMethodOrder(OrderAnnotation.class)
public class MultiMessagesTest
{
//
//   private static final String TEST1BUNDLE_PATH = "Test1";
//   private static final String TEST1BUNDLE_KEY = "Test1Bundle.key1";
//   private static final String TEST1BUNDLE_VALUE = "My Test1 bundle value";
//   private static final String TEST1BUNDLE_ARGSKEY = "Test1Bundle.key2";
//   private static final String TEST1BUNDLE_ARGSVALUE = "My Test1 bundle value with arg1 : 1, arg2: 1.23 and arg3: Succeeded";
//   private static final String TEST1BUNDLE_INTKEY = "Test1Bundle.key3";
//   private static final String TEST1BUNDLE_DOUBLEKEY = "Test1Bundle.key4";
//   private static final String TEST1FIRSTBUNDLE_PATH = "Test1First";
//   private static final String TEST1FIRSTBUNDLE_VALUE = "My Test1 first bundle value";
//
//   private static final String TEST2DEFAULTBUNDLE_PATH = "Test2Default";
//   private static final String TEST2BUNDLE_PATH = "Test2";
//   private static final String TEST2BUNDLE_KEY = "Test2Bundle.key2";
//   private static final String TEST2DEFAULTBUNDLE_VALUE = "My Test2 default bundle value";
//   private static final String TEST2BUNDLE_VALUE = "My Test2 bundle value";
//
//   private static final String TEST3BUNDLE_PATH = "Test3";
//   private static final String TEST3BUNDLE_KEY = "Test3Bundle.key";
//   private static final String TEST3BUNDLE_VALUE = "My Test3 bundle value";
//
//   private static final String ASSOCIATEDTESTBUNDLE_KEY = "AssociatedTestBundle.key1";
//   private static final String ASSOCIATEDTESTBUNDLE_VALUE = "My Associated Test1 bundle value";
//
//   private static final String TESTAPPLICATIVEKEY1_KEY = "Test.applicativeKey1";
//   private static final String TESTAPPLICATIVEKEY1_VALUE = "My applicative value";
//
//   private static final String TESTPROVIDERKEY1_KEY = "MyProvider.key";
//   private static final String TESTPROVIDERKEY1_VALUE = "This is my value";
//
//   /**
//    * Gets locale test.
//    */
//   @Test
//   @Order(1)
//   void getLocaleTest()
//   {
//       Locale locale = MultiMessages.getLocale();
//       Assertions.assertNotNull(locale);
//       Assertions.assertEquals(Locale.getDefault(), locale);
//   }
//
//   /**
//    * Add applicative key.
//    */
//   @Order(101)
//   @Test
//   void addApplicativeKeyTest()
//   {
//      MultiMessages.addApplicativeKey(TESTAPPLICATIVEKEY1_KEY,TESTAPPLICATIVEKEY1_VALUE);
//      String value = MultiMessages.get(TESTAPPLICATIVEKEY1_KEY);
//      Assertions.assertEquals(TESTAPPLICATIVEKEY1_VALUE, value);
//   }
//
//   /**
//    * Remove applicative key.
//    */
//   @Order(102)
//   @Test
//   void removeApplicativeKeyTest()
//   {
//      // Verify current value for the key
//      String value = MultiMessages.get(TESTAPPLICATIVEKEY1_KEY);
//      Assertions.assertEquals(TESTAPPLICATIVEKEY1_VALUE, value);
//
//      // remove key and verify
//      MultiMessages.removeApplicativeKey(TESTAPPLICATIVEKEY1_KEY);
//      value = MultiMessages.get(TESTAPPLICATIVEKEY1_KEY);
//      Assertions.assertEquals(TESTAPPLICATIVEKEY1_KEY, value);
//   }
//
//   /**
//    * Add associated bundle.
//    */
//   @Test
//   @Order(31)
//   void addAssociatedBundle()
//   {
//      // Verify current (default) value
//      String value = MultiMessages.get(ASSOCIATEDTESTBUNDLE_KEY);
//      Assertions.assertEquals(ASSOCIATEDTESTBUNDLE_KEY, value);
//
//      // Load bundle
//      boolean res = MultiMessages.addAssociatedBundle(MultiMessagesTest.class);
//      Assertions.assertTrue(res);
//
//      // Verify new key value
//      value = MultiMessages.get(ASSOCIATEDTESTBUNDLE_KEY);
//      Assertions.assertEquals(ASSOCIATEDTESTBUNDLE_VALUE, value);
//   }
//
//   /**
//    * Add bundle.
//    */
//   @Test
//   @Order(11)
//   void addBundle()
//   {
//      // Verify current (default) value
//      String value = MultiMessages.get(TEST1BUNDLE_KEY);
//      Assertions.assertEquals(TEST1BUNDLE_KEY, value);
//
//      // Load bundle
//      boolean res = MultiMessages.addBundle(TEST1BUNDLE_PATH);
//      Assertions.assertTrue(res);
//
//      // Verify new key value
//      value = MultiMessages.get(TEST1BUNDLE_KEY);
//      Assertions.assertEquals(TEST1BUNDLE_VALUE, value);
//   }
//
//   /**
//    * Add provider.
//    */
//   @Test
//   @Order(201)
//   void addProvider()
//   {
//      // Verify current (default) value
//      String value = MultiMessages.get(TESTPROVIDERKEY1_KEY);
//      Assertions.assertEquals(TESTPROVIDERKEY1_KEY, value);
//
//      MultiMessages.addProvider(new TestProvider());
//
//      value = MultiMessages.get(TESTPROVIDERKEY1_KEY);
//      Assertions.assertEquals(TESTPROVIDERKEY1_VALUE, value);
//   }
//
//   /**
//    * Sets first bundle.
//    */
//   @Test
//   @Order(21)
//   void setFirstBundle()
//   {
//      // Verify current (default) value
//      String value = MultiMessages.get(TEST1BUNDLE_KEY);
//      Assertions.assertEquals(TEST1BUNDLE_VALUE, value);
//
//      // Load first bundle
//      MultiMessages.setFirstBundle(TEST1FIRSTBUNDLE_PATH);
//
//      // Verify new key value
//      value = MultiMessages.get(TEST1BUNDLE_KEY);
//      Assertions.assertEquals(TEST1FIRSTBUNDLE_VALUE, value);
//   }
//
//   /**
//    * Sets default bundle.
//    */
//   @Test
//   @Order(22)
//   void setDefaultBundle()
//   {
//      // Verify current (default) value
//      String value = MultiMessages.get(TEST2BUNDLE_KEY);
//      Assertions.assertEquals(TEST2BUNDLE_KEY, value);
//
//      // Load default bundle
//      MultiMessages.setFirstBundle(TEST2DEFAULTBUNDLE_PATH);
//
//      // Verify new key value
//      value = MultiMessages.get(TEST2BUNDLE_KEY);
//      Assertions.assertEquals(TEST2DEFAULTBUNDLE_VALUE, value);
//
//      // Load a bundle
//      MultiMessages.setFirstBundle(TEST2BUNDLE_PATH);
//
//      // Verify new key value
//      value = MultiMessages.get(TEST2BUNDLE_KEY);
//      Assertions.assertEquals(TEST2BUNDLE_VALUE, value);
//   }
//
////   /**
////    * Gets bundle.
////    */
////   @Test
////   @Order(41)
////   void getObjectBundleTest()
////   {
////      Assertions.fail("Not implemented");
////   }
////
////   /**
////    * Test get bundle.
////    */
////   @Test
////   @Order(42)
////   void getClassBundleTest()
////   {
////      Assertions.fail("Not implemented");
////   }
//
//   /**
//    * Test get bundle 1.
//    */
//   @Test
//   @Order(43)
//   void getPathBundleTest()
//   {
//      // Load bundle
//      ResourceBundle bundle = MultiMessages.getBundle(TEST3BUNDLE_PATH);
//      Assertions.assertNotNull(bundle);
//
//      // Verify new key value
//      String value = bundle.getString(TEST3BUNDLE_KEY);
//      Assertions.assertEquals(TEST3BUNDLE_VALUE, value);
//   }
//
//   /**
//    * Gets as int.
//    */
//   @Test
//   @Order(14)
//   void getAsInt()
//   {
//      int value = MultiMessages.getAsInt(TEST1BUNDLE_INTKEY);
//      Assertions.assertEquals(1234, value);
//   }
//
//   /**
//    * Gets as double.
//    */
//   @Test
//   @Order(13)
//   void getAsDouble()
//   {
//      double value = MultiMessages.getAsDouble(TEST1BUNDLE_DOUBLEKEY);
//      Assertions.assertEquals(1.234, value);
//   }
//
//   /**
//    * Get.
//    */
//   @Test
//   @Order(12)
//   void get()
//   {
//      // Verify simple key value
//      String value = MultiMessages.get(TEST1BUNDLE_KEY);
//      Assertions.assertEquals(TEST1BUNDLE_VALUE, value);
//
//      // Verify key value with arguments
//      value = MultiMessages.get(TEST1BUNDLE_ARGSKEY, "1", "1.23", "Succeeded");
//      Assertions.assertEquals(TEST1BUNDLE_ARGSVALUE, value);
//   }
}