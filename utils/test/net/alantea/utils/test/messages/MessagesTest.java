package net.alantea.utils.test.messages;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;

import net.alantea.utils.MultiMessages;

public class MessagesTest
{
   @Test
   public void simpleMessageTest()
   {
      boolean added = MultiMessages.addBundle("net.alantea.utils.MessagesTest");
      Assertions.assertTrue(added);
      String text = MultiMessages.get("Encoding.test1");
      Assertions.assertEquals("éèàçù", text);
   }

}
