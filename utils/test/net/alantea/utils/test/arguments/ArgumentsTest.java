package net.alantea.utils.test.arguments;

import org.apache.commons.cli.ParseException;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;

import net.alantea.utils.ArgumentDescription;
import net.alantea.utils.ArgumentName;
import net.alantea.utils.ArgumentShortName;
import net.alantea.utils.Utils;


/**
 * The Class ArgumentsTest.
 */
public class ArgumentsTest
{
   
   /** The Constant TESTZERO. */
   private static final String[] TESTZERO = {""};
   
   /** The Constant TESTONETRUE. */
   private static final String[] TESTONETRUE = {"-test1"};
   
   /** The Constant TESTONEFALSE. */
   private static final String[] TESTONEFALSE = {""};

   /** The Constant TESTTWOINITIAL. */
   private static final String TESTTWOINITIAL = "Initial";
   
   /** The Constant TESTTWOEXPECTED. */
   private static final String TESTTWOEXPECTED = "NEWVALUE";
   
   /** The Constant TESTTWOCMD. */
   private static final String[] TESTTWOCMD = {"-test2", TESTTWOEXPECTED};

   /** The Constant TESTTHREETRUE. */
   private static final String[] TESTTHREETRUE = {"-test3"};
   
   /** The Constant TESTTHREEFALSE. */
   private static final String[] TESTTHREEFALSE = {""};

   /** The Constant TESTFOURINITIAL. */
   private static final String TESTFOURINITIAL = "Initial";
   
   /** The Constant TESTFOUREXPECTED. */
   private static final String TESTFOUREXPECTED = "NEWVALUE";
   
   /** The Constant TESTFOURCMD. */
   private static final String[] TESTFOURCMD = {"-test4", TESTFOUREXPECTED};
   
   /** The Constant TESTFIFTH1. */
   private static final String[] TESTFIVE1 = {"-option1"};
   
   /** The Constant TESTFIFTH2. */
   private static final String[] TESTFIVE2 = {"-option2"};
   
   /** The Constant TESTFIFTH3. */
   private static final String[] TESTFIVE3 = {"-option3"};
   
   /** The Constant TESTFIFTH1. */
   private static final String[] TESTFIVE4 = {"-option1", "-option3"};

   /** The Constant TESTSIXINITIAL1. */
   private static final String TESTSIXINITIAL1 = "Initial1";
   
   /** The Constant TESTSIXINITIAL2. */
   private static final String TESTSIXINITIAL2 = "Initial2";
   
   /** The Constant TESTSIXINITIAL3. */
   private static final String TESTSIXINITIAL3 = "Initial3";
   
   /** The Constant TESTSIXEXPECTED1. */
   private static final String TESTSIXEXPECTED1 = "NEWVALUE1";
   
   /** The Constant TESTSIXEXPECTED2. */
   private static final String TESTSIXEXPECTED2 = "NEWVALUE2";
   
   /** The Constant TESTSIXEXPECTED3. */
   private static final String TESTSIXEXPECTED3 = "NEWVALUE3";
   
   /** The Constant TESTSIXCMD1. */
   private static final String[] TESTSIXCMD1 = {"-option1", TESTSIXEXPECTED1};
   
   /** The Constant TESTSIXCMD2. */
   private static final String[] TESTSIXCMD2 = {"-option2", TESTSIXEXPECTED2};
   
   /** The Constant TESTSIXCMD3. */
   private static final String[] TESTSIXCMD3 = {"-option3", TESTSIXEXPECTED3};
   
   /** The Constant TESTSIXCMD4. */
   private static final String[] TESTSIXCMD4 = {"-option1", TESTSIXEXPECTED1, "-option3", TESTSIXEXPECTED3};

   /** The Constant TESTSEVENINITIAL1. */
   private static final String TESTSEVENINITIAL1 = "Initial1";
   
   /** The Constant TESTSEVENINITIAL2. */
   private static final String TESTSEVENINITIAL2 = "Initial2";
   
   /** The Constant TESTSEVENINITIAL3. */
   private static final String TESTSEVENINITIAL3 = "Initial3";
   
   /** The Constant TESTSEVENEXPECTED1. */
   private static final String TESTSEVENEXPECTED1 = "NEWVALUE1";
   
   /** The Constant TESTSEVENEXPECTED2. */
   private static final String TESTSEVENEXPECTED2 = "NEWVALUE2";
   
   /** The Constant TESTSEVENEXPECTED3. */
   private static final String TESTSEVENEXPECTED3 = "NEWVALUE3";
   
   /** The Constant TESTSEVENCMD1. */
   private static final String[] TESTSEVENCMD1 = {"-option1"};
   
   /** The Constant TESTSEVENCMD2. */
   private static final String[] TESTSEVENCMD2 = {"-option2"};
   
   /** The Constant TESTSEVENCMD3. */
   private static final String[] TESTSEVENCMD3 = {"-option3"};
   
   /** The Constant TESTSEVENCMD4. */
   private static final String[] TESTSEVENCMD4 = {"-value1", TESTSIXEXPECTED1};
   
   /** The Constant TESTSEVENCMD5. */
   private static final String[] TESTSEVENCMD5 = {"-value2", TESTSIXEXPECTED2};
   
   /** The Constant TESTSEVENCMD6. */
   private static final String[] TESTSEVENCMD6 = {"-value3", TESTSIXEXPECTED3};
   
   /** The Constant TESTSEVENCMD7. */
   private static final String[] TESTSEVENCMD7 = {"-value1", TESTSIXEXPECTED1, "-value3", TESTSIXEXPECTED3};
   
   /** The Constant TESTSEVENCMD8. */
   private static final String[] TESTSEVENCMD8 = {"-option2", "-value3", TESTSIXEXPECTED3};
   
   /** The Constant TESTSEVENCMD9. */
   private static final String[] TESTSEVENCMD9 = {"-option1", "-option2", "-option3",
      "-value1", TESTSIXEXPECTED1, "-value2", TESTSIXEXPECTED2, "-value3", TESTSIXEXPECTED3};
   
   /** The Constant TESTSEVENCMD10. */
   private static final String[] TESTSEVENCMD10 = {"-value3", TESTSIXEXPECTED3, "-value2", TESTSIXEXPECTED2,
      "-value1", TESTSIXEXPECTED1, "-option3", "-option2", "-option1"};

   
   /** The Constant TESTHEIGHTCMD. */
   private static final String[] TESTHEIGHTCMD = {"-t"};
   
   /** The Constant TESTNINECMD. */
   private static final String[] TESTNINECMD = {"-t", TESTTWOEXPECTED};
   
   private static final String[] TESTTENCMD1 = {"-erroneous"};
   private static final String[] TESTTENCMD2 = {"-value1", TESTSIXEXPECTED1, "-erroneous"};
   
   /**
    *  Empty arguments test.
    */
   @Test
   public void emptyArgumentsTest()
   {
      Arguments0 argument = new Arguments0();
      Assertions.assertNotNull(argument);
      Assertions.assertFalse(argument.test0);

      argument = new Arguments0();
      Utils.securedParseArguments(argument, TESTZERO);
      Assertions.assertFalse(argument.test0);
   }
   
   /**
    * Simple boolean arguments test.
    */
   @Test
   public void simpleBooleanArgumentsTest()
   {
      Arguments1 argument = new Arguments1();
      Assertions.assertNotNull(argument);
      Assertions.assertFalse(argument.test1);
      
      Utils.securedParseArguments(argument, TESTONETRUE);
      Assertions.assertTrue(argument.test1);

      argument = new Arguments1();
      Utils.securedParseArguments(argument, TESTONEFALSE);
      Assertions.assertFalse(argument.test1);
   }

   /**
    * Simple string arguments test.
    */
   @Test
   public void simpleStringArgumentsTest()
   {
      Arguments2 argument = new Arguments2();
      Assertions.assertNotNull(argument);
      Assertions.assertEquals(TESTTWOINITIAL, argument.test2);
      
      Utils.securedParseArguments(argument, TESTTWOCMD);
      Assertions.assertEquals(TESTTWOEXPECTED, argument.test2);
   }
   
   /**
    * Simple named boolean arguments test.
    */
   @Test
   public void simpleNamedBooleanArgumentsTest()
   {
      Arguments3 argument = new Arguments3();
      Assertions.assertNotNull(argument);
      Assertions.assertFalse(argument.mytest3);
      
      Utils.securedParseArguments(argument, TESTTHREETRUE);
      Assertions.assertTrue(argument.mytest3);

      argument = new Arguments3();
      Utils.securedParseArguments(argument, TESTTHREEFALSE);
      Assertions.assertFalse(argument.mytest3);
   }

   /**
    * Simple named string arguments test.
    */
   @Test
   public void simpleNamedStringArgumentsTest()
   {
      Arguments4 argument = new Arguments4();
      Assertions.assertNotNull(argument);
      Assertions.assertEquals(TESTFOURINITIAL, argument.mytest4);
      
      Utils.securedParseArguments(argument, TESTFOURCMD);
      Assertions.assertEquals(TESTFOUREXPECTED, argument.mytest4);
   }
   
   /**
    * Multiple named boolean arguments test.
    */
   @Test
   public void multipleNamedBooleanArgumentsTest()
   {
      Arguments5 argument = new Arguments5();
      Assertions.assertNotNull(argument);
      Assertions.assertFalse(argument.myoption1);
      Assertions.assertFalse(argument.myoption2);
      Assertions.assertFalse(argument.myoption3);
      
      Utils.securedParseArguments(argument, TESTFIVE1);
      Assertions.assertTrue(argument.myoption1);
      Assertions.assertFalse(argument.myoption2);
      Assertions.assertFalse(argument.myoption3);

      argument = new Arguments5();
      Utils.securedParseArguments(argument, TESTFIVE2);
      Assertions.assertFalse(argument.myoption1);
      Assertions.assertTrue(argument.myoption2);
      Assertions.assertFalse(argument.myoption3);

      argument = new Arguments5();
      Utils.securedParseArguments(argument, TESTFIVE3);
      Assertions.assertFalse(argument.myoption1);
      Assertions.assertFalse(argument.myoption2);
      Assertions.assertTrue(argument.myoption3);

      argument = new Arguments5();
      Utils.securedParseArguments(argument, TESTFIVE4);
      Assertions.assertTrue(argument.myoption1);
      Assertions.assertFalse(argument.myoption2);
      Assertions.assertTrue(argument.myoption3);
   }

   /**
    * Multiple named string arguments test.
    */
   @Test
   public void multipleNamedStringArgumentsTest()
   {
      Arguments6 argument = new Arguments6();
      Assertions.assertNotNull(argument);
      Assertions.assertEquals(TESTSIXINITIAL1, argument.myoption1);
      Assertions.assertEquals(TESTSIXINITIAL2, argument.myoption2);
      Assertions.assertEquals(TESTSIXINITIAL3, argument.myoption3);
      
      Utils.securedParseArguments(argument, TESTSIXCMD1);
      Assertions.assertEquals(TESTSIXEXPECTED1, argument.myoption1);
      Assertions.assertEquals(TESTSIXINITIAL2, argument.myoption2);
      Assertions.assertEquals(TESTSIXINITIAL3, argument.myoption3);
      
      argument = new Arguments6();
      Utils.securedParseArguments(argument, TESTSIXCMD2);
      Assertions.assertEquals(TESTSIXINITIAL1, argument.myoption1);
      Assertions.assertEquals(TESTSIXEXPECTED2, argument.myoption2);
      Assertions.assertEquals(TESTSIXINITIAL3, argument.myoption3);
      
      argument = new Arguments6();
      Utils.securedParseArguments(argument, TESTSIXCMD3);
      Assertions.assertEquals(TESTSIXINITIAL1, argument.myoption1);
      Assertions.assertEquals(TESTSIXINITIAL2, argument.myoption2);
      Assertions.assertEquals(TESTSIXEXPECTED3, argument.myoption3);
      
      argument = new Arguments6();
      Utils.securedParseArguments(argument, TESTSIXCMD4);
      Assertions.assertEquals(TESTSIXEXPECTED1, argument.myoption1);
      Assertions.assertEquals(TESTSIXINITIAL2, argument.myoption2);
      Assertions.assertEquals(TESTSIXEXPECTED3, argument.myoption3);
   }

   /**
    * Multiple named string arguments test.
    */
   @Test
   public void completeNamedStringArgumentsTest()
   {
      Arguments7 argument = new Arguments7();
      Assertions.assertNotNull(argument);
      Assertions.assertFalse(argument.myoption1);
      Assertions.assertFalse(argument.myoption2);
      Assertions.assertFalse(argument.myoption3);
      Assertions.assertEquals(TESTSEVENINITIAL1, argument.myvalue1);
      Assertions.assertEquals(TESTSEVENINITIAL2, argument.myvalue2);
      Assertions.assertEquals(TESTSEVENINITIAL3, argument.myvalue3);
      
      Utils.securedParseArguments(argument, TESTSEVENCMD1);
      Assertions.assertTrue(argument.myoption1);
      Assertions.assertFalse(argument.myoption2);
      Assertions.assertFalse(argument.myoption3);
      Assertions.assertEquals(TESTSEVENINITIAL1, argument.myvalue1);
      Assertions.assertEquals(TESTSEVENINITIAL2, argument.myvalue2);
      Assertions.assertEquals(TESTSEVENINITIAL3, argument.myvalue3);
      
      argument = new Arguments7();
      Utils.securedParseArguments(argument, TESTSEVENCMD2);
      Assertions.assertFalse(argument.myoption1);
      Assertions.assertTrue(argument.myoption2);
      Assertions.assertFalse(argument.myoption3);
      Assertions.assertEquals(TESTSEVENINITIAL1, argument.myvalue1);
      Assertions.assertEquals(TESTSEVENINITIAL2, argument.myvalue2);
      Assertions.assertEquals(TESTSEVENINITIAL3, argument.myvalue3);
      
      argument = new Arguments7();
      Utils.securedParseArguments(argument, TESTSEVENCMD3);
      Assertions.assertFalse(argument.myoption1);
      Assertions.assertFalse(argument.myoption2);
      Assertions.assertTrue(argument.myoption3);
      Assertions.assertEquals(TESTSEVENINITIAL1, argument.myvalue1);
      Assertions.assertEquals(TESTSEVENINITIAL2, argument.myvalue2);
      Assertions.assertEquals(TESTSEVENINITIAL3, argument.myvalue3);
      
      argument = new Arguments7();
      Utils.securedParseArguments(argument, TESTSEVENCMD4);
      Assertions.assertFalse(argument.myoption1);
      Assertions.assertFalse(argument.myoption2);
      Assertions.assertFalse(argument.myoption3);
      Assertions.assertEquals(TESTSEVENEXPECTED1, argument.myvalue1);
      Assertions.assertEquals(TESTSEVENINITIAL2, argument.myvalue2);
      Assertions.assertEquals(TESTSEVENINITIAL3, argument.myvalue3);
      
      argument = new Arguments7();
      Utils.securedParseArguments(argument, TESTSEVENCMD5);
      Assertions.assertFalse(argument.myoption1);
      Assertions.assertFalse(argument.myoption2);
      Assertions.assertFalse(argument.myoption3);
      Assertions.assertEquals(TESTSEVENINITIAL1, argument.myvalue1);
      Assertions.assertEquals(TESTSEVENEXPECTED2, argument.myvalue2);
      Assertions.assertEquals(TESTSEVENINITIAL3, argument.myvalue3);
      
      argument = new Arguments7();
      Utils.securedParseArguments(argument, TESTSEVENCMD6);
      Assertions.assertFalse(argument.myoption1);
      Assertions.assertFalse(argument.myoption2);
      Assertions.assertFalse(argument.myoption3);
      Assertions.assertEquals(TESTSEVENINITIAL1, argument.myvalue1);
      Assertions.assertEquals(TESTSEVENINITIAL2, argument.myvalue2);
      Assertions.assertEquals(TESTSEVENEXPECTED3, argument.myvalue3);
      
      argument = new Arguments7();
      Utils.securedParseArguments(argument, TESTSEVENCMD7);
      Assertions.assertFalse(argument.myoption1);
      Assertions.assertFalse(argument.myoption2);
      Assertions.assertFalse(argument.myoption3);
      Assertions.assertEquals(TESTSEVENEXPECTED1, argument.myvalue1);
      Assertions.assertEquals(TESTSEVENINITIAL2, argument.myvalue2);
      Assertions.assertEquals(TESTSEVENEXPECTED3, argument.myvalue3);
      
      argument = new Arguments7();
      Utils.securedParseArguments(argument, TESTSEVENCMD8);
      Assertions.assertFalse(argument.myoption1);
      Assertions.assertTrue(argument.myoption2);
      Assertions.assertFalse(argument.myoption3);
      Assertions.assertEquals(TESTSEVENINITIAL1, argument.myvalue1);
      Assertions.assertEquals(TESTSEVENINITIAL2, argument.myvalue2);
      Assertions.assertEquals(TESTSEVENEXPECTED3, argument.myvalue3);
      
      argument = new Arguments7();
      Utils.securedParseArguments(argument, TESTSEVENCMD9);
      Assertions.assertTrue(argument.myoption1);
      Assertions.assertTrue(argument.myoption2);
      Assertions.assertTrue(argument.myoption3);
      Assertions.assertEquals(TESTSEVENEXPECTED1, argument.myvalue1);
      Assertions.assertEquals(TESTSEVENEXPECTED2, argument.myvalue2);
      Assertions.assertEquals(TESTSEVENEXPECTED3, argument.myvalue3);
      
      argument = new Arguments7();
      Utils.securedParseArguments(argument, TESTSEVENCMD10);
      Assertions.assertTrue(argument.myoption1);
      Assertions.assertTrue(argument.myoption2);
      Assertions.assertTrue(argument.myoption3);
      Assertions.assertEquals(TESTSEVENEXPECTED1, argument.myvalue1);
      Assertions.assertEquals(TESTSEVENEXPECTED2, argument.myvalue2);
      Assertions.assertEquals(TESTSEVENEXPECTED3, argument.myvalue3);
   }
   
   /**
    * Simple boolean arguments test.
    */
   @Test
   public void otherSimpleBooleanArgumentsTest()
   {
      Arguments8 argument = new Arguments8();
      Assertions.assertNotNull(argument);
      Assertions.assertFalse(argument.mytest);
      
      Utils.securedParseArguments(argument, TESTHEIGHTCMD);
      Assertions.assertTrue(argument.mytest);
   }

   /**
    * Simple string arguments test.
    */
   @Test
   public void otherSimpleStringArgumentsTest()
   {
      Arguments9 argument = new Arguments9();
      Assertions.assertNotNull(argument);
      Assertions.assertEquals(TESTTWOINITIAL, argument.mytest);
      
      Utils.securedParseArguments(argument, TESTNINECMD);
      Assertions.assertEquals(TESTTWOEXPECTED, argument.mytest);
   }

   /**
    * Erroneous arguments test.
    */
   @Test
   public void erroneousArgumentsTest()
   {
      Arguments7 argument = new Arguments7();
      
      try
      {
         Utils.parseArguments(argument, TESTTENCMD1);
         Assertions.fail("erroneous argument not detected");
      }
      catch (IllegalArgumentException | IllegalAccessException | ParseException e)
      {
         // this is correct !
      }
      
      try
      {
         Utils.parseArguments(argument, TESTTENCMD2);
         Assertions.fail("erroneous argument not detected");
      }
      catch (IllegalArgumentException | IllegalAccessException | ParseException e)
      {
         // this is correct !
      }
   }
   
   /**
    * The Class Arguments0.
    */
   static class Arguments0
   {
      
      /** The test 0. */
      boolean test0 = false;
   }   
   
   /**
    * The Class Arguments1.
    */
   static class Arguments1
   {
      
      /** The test 1. */
      @ArgumentName()
      boolean test1 = false;
   }   
   
   /**
    * The Class Arguments2.
    */
   static class Arguments2
   {
      
      /** The test 2. */
      @ArgumentName()
      String test2 = TESTTWOINITIAL;
   }   
   
   /**
    * The Class Arguments3.
    */
   static class Arguments3
   {
      
      /** The mytest 3. */
      @ArgumentName("test3")
      boolean mytest3 = false;
   }   
   
   /**
    * The Class Arguments4.
    */
   static class Arguments4
   {
      
      /** The mytest 4. */
      @ArgumentName("test4")
      String mytest4 = TESTFOURINITIAL;
   }   
   
   /**
    * The Class Arguments5.
    */
   static class Arguments5
   {
      /** The myoption1. */
      @ArgumentName("option1")
      boolean myoption1 = false;
      
      /** The myoption2. */
      @ArgumentName("option2")
      boolean myoption2 = false;
      
      /** The myoption3. */
      @ArgumentName("option3")
      boolean myoption3 = false;
   } 
   
   /**
    * The Class Arguments6.
    */
   static class Arguments6
   {
      /** The myoption1. */
      @ArgumentName("option1")
      String myoption1 = TESTSIXINITIAL1;
      
      /** The myoption2. */
      @ArgumentName("option2")
      String myoption2 = TESTSIXINITIAL2;
      
      /** The myoption3. */
      @ArgumentName("option3")
      String myoption3 = TESTSIXINITIAL3;
   }
   
   /**
    * The Class Arguments7.
    */
   static class Arguments7
   {
      /** The myoption1. */
      @ArgumentName("option1")
      boolean myoption1 = false;
      
      /** The myoption2. */
      @ArgumentName("option2")
      boolean myoption2 = false;
      
      /** The myoption3. */
      @ArgumentName("option3")
      boolean myoption3 = false;
      
      /** The myvalue1. */
      @ArgumentName("value1")
      String myvalue1 = TESTSEVENINITIAL1;
      
      /** The myvalue2. */
      @ArgumentName("value2")
      String myvalue2 = TESTSEVENINITIAL2;
      
      /** The myvalue3. */
      @ArgumentName("value3")
      String myvalue3 = TESTSEVENINITIAL3;
   }   
   
   /**
    * The Class Arguments8.
    */
   static class Arguments8
   {
      
      /** The test. */
      @ArgumentName("test")
      @ArgumentShortName("t")
      boolean mytest = false;
   }   
   
   /**
    * The Class Arguments9.
    */
   static class Arguments9
   {
      
      /** The test. */
      @ArgumentName("test")
      @ArgumentShortName("t")
      @ArgumentDescription("test option description")
      String mytest = TESTTWOINITIAL;
   }   
}
