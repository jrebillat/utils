# Utils
This package contains - and will contain more - utilities classes used by the other Alantea packages.
Most functionalities are still in alpha stage.


## Messages
A common problem is to manage internationalization in Strings, either for GUI, logging, file output or console output. The Java response is to offer access to properties files that contains key-values pairs of Strings and that may be internationalized, naming it a *bundle*.

Most of the time, a package has its own bundle, stored internally, that manage the strings for this package. This is good, as far as a package will not offer packages using the packages functionalities to modify its personal strings. In this case, the higher level package must have access to the internal bundle.

To offer such a possibility, but controlled, the Utils package offers a MultiMessages class that is able to manage loading and accessing multiple bundles at once and thus offers all packages to access all of the stored keys, in any bundle.

One problem is the priority of bundles between them. As it is not possible to define which bundle will be loaded before another, it is - for the moment - decided by the order they are inserted : last inserted is searched first. But the messaging class will manage two special cases : the first bundle to search in and the latest bundle to search - the catch-all case. Those two will remain at their position regardless of what is inserted afterwards. 

Bundles are technically named files contained in the class path (in the src package hierarchy, in any resources directory or in a jar). The bundle may be found using a java-like name (package.name) or through a class that will have the same signature as the bundle files.

Applications may also need to manage those elements in different ways (not using bundles). We offer here some approaches for such management.

### First bundle
The first bundle has to be set by the application. This one will be search before any other bundle. Its keys will thus override all other definitions of the same keys in other bundles. Beware : if several bundles are set as "first", the latest one wins and the others are going done in the list !

### latest bundle
The latest bundle may to be set by the application or a package. Be careful : there is just one latest package. Defining a latest package will forget the previously defined one. This bundle will be search after any other bundle. Its keys may thus be overridden by any other definitions of the same keys in other bundles.

### Applicative keys
The applicative keys are key/value pairs managed like the bundle's keys, and thus accessed as if they were in a bundle that is searched before the first bundle. These values are not internationalized but may be calculated by the application and even modified during execution.

### Providers
A message provider in a class that offers same capabilities as bundles : searching for a key corresponding value depending on the locale language. Those classes are created and managed in the application and are searched just after the first bundle.

### MultiMessages
The MultiMessages class offers the following static methods for standard bundles use :
- `public static boolean addBundle(String path)` will add the bundle in the bundles pool.
- `public static boolean addAssociatedBundle(Object associated)` will add the bundle which name and package are the same as the given object class.
- `public static void setFirstBundle(String path)` will set the bundle as first bundle. It has not to have been added before.
- `public static void setLatestBundle(String path)` will set the bundle as latest bundle. It has not to have been added before.
- `public static String get(String key, String... args)` will search for the key in the known bundles, starting by first bundle and ending with latest. It will return the found value or the key if no value has been found. It will search the value for patterns like [1] or [2],... and replace them by the corresponding argument if any has been given (arguments numbers are starting at 1).

There are also methods for special accesses (applicative keys and providers):
- `public static void addApplicativeKey(String key, String value)` will add the key value in the keys pool.
- `public static void removeApplicativeKey(String key)` will remove the key value from the keys pool.
- `public static void addProvider(MessageProvider provider)` will add the provider in the providers pool.

In special cases, the coder wants to manage himself the bundle for his code. There are some methods to do that :
- `public static ResourceBundle getBundle(String path)` will add the bundle in the bundles pool.
- `public static ResourceBundle getBundle(Class reference)` will add the bundle which name and package are the same as the given class in the bundles pool.
- `public static ResourceBundle getBundle(Object object)` will add the bundle which name and package are the same as the given object class in the bundles pool.
- `public static Locale getLocale()` returns the current Locale.

### MessageProvider
A message provider is a class deriving from the MessageProvider class. They have to implement methods in the form of 
`public String get_fr_FR(String key, String... args)` with the method names containing the actual local they manage.

A default `public String get(String key, String... args)` must exist to manage unknown locales.

Note: this functionality is in alpha stage.

## Localization

### LocalizedString
The LocalizedString class will simplify handling of internationalization in Strings. It is built with :
- `public LocalizedString(String key, String... args)` and will contains the localized value of the key, as found in the bundles.

The contained method is :
- `public String getKey()` will return the key.

The class also override the `toString`, `compare`, `equals` and `hashCode` methods.

### I18N
I18N stands foe Internationalization.
Set over MultiMessages, it offers a shortcut :
- `public static final String get(String resourceKey, String... args)` is just a shortcut for the same method in MultiMessages.

### Locale information
This class offers several methods to get information about the Locale.
- `public static String getCountryCode()` will return the Locale code for the current Locale.
- `public static String getCountryCode(Locale locale)` will return the Locale code for the given Locale.
- `public static String getDisplayCountry()` will return the name of the country for the current Locale.
- `public static String getDisplayCountry(Locale locale)` will return the name of the country for the given Locale.

### Date and Time
It offers many Date and Time methods.
- `public static String getDate(Date date)` will return the default String for the Date.
- `public static String getShortDate(Date date)` will return the short String for the Date.
- `public static String getMediumDate(Date date)` will return the medium String for the Date.
- `public static String getLongDate(Date date)` will return the long String for the Date.
- `public static String getTime(Date date)` will return the default String for the Time.
- `public static String getShortTime(Date date)` will return the short String for the Time.
- `public static String getMediumTime(Date date)` will return the medium String for the Time.
- `public static String getLongTime(Date date)` will return the long String for the Time.
- `public static String getDateTime(Date date)` will return the default String for the Date and Time.
- `public static String getShortDateTime(Date date)` will return the short String for the Date and Time.
- `public static String getMediumDateTime(Date date)` will return the medium String for the Date and Time.
- `public static String getLongDateTime(Date date)` will return the long String for the Date and Time.
- `public static String getDateTime(Date date, int dateFormat, int timeFormat)` will return the String for the Date and Time corresponding to the two given DateFormat constants.
- `public static String getDateTime(Date date, String formatString)` will return the  String for the Date and Time depending on the given Date format.


## Exception management
The LntException is there to be the base class for all exceptions in the Alantea packages. It should be derived or directly used in other packages. 
An LntException has a severity level (using enum LntException.LEVEL) that may be one of
- LOG_INFO
- LOG_WARN
- LOG_DEBUG
- LOG-ERROR

A LntException logs its message with its severity level if a slf4j logger exists.

### LntException
Such an exception may be created with :
- `public LntException(String message)` create an exception with the level LOG_WARN
- `public LntException(String message, Throwable source)` create an exception with the level LOG_WARN
- `public LntException(String message, LEVEL level)`
- `public LntException(String message, LEVEL level, Throwable source)`

The last 10 created LntExceptions - not only the ones that were thrown - are automatically saved in a list.
The exceptions list may be got with the static method :
- `public static List<LntException> popExceptions()` that will return the current list of LnTExceptions, and clear the list of exceptions.
- `public static List<LntException> peekExceptions()` that will return the current list of LnTExceptions .
- `public static LntException pop()` that will return the first LnTException in the list, and remove it from the list.
- `public static LntException peek()` that will return the first LnTException in the list.

From any LntException, it is possible to get its level:
- `public LEVEL getLevel()` that will return the LnTException level.

## Color names
The ColorNames class gives simplified access to standard color names, web codes and AWT Color values.The methods are:
- `public static String getColorCode(String name)` that will return the web color code that corresponds to the given color name.
- `public static Color getColor(String name)` that will return the AWT Color object corresponding to the given name.
- `public static List<String> getColorNames()` that will return the list of known color names.
- `public static void addColor(String name, String code)` that will add a new name and associate a web color code to it.
- `public static void changeColor(String name, String code)` that will change the web color code associated to a given color name.
- `public static int getRGB(Color color)` that will return the RGB standard integer value for that color.
- `public static String getRGBCode(Color color)` that will return the RGB standard web value (starting with a # sign) for that color.

## Images
This class offers access to image manipulation.The method is:
- `public static Image resizeImage(BufferedImage original, int maxw, int maxh)` that will return the image, resized to best fit in the given size.
- `public static Image resizeImageToHeight(BufferedImage original, int maxh)` that will return the image, resized to best fit in the given height.
- `public static Image resizeImageToWidth(BufferedImage original, int maxw)` that will return the image, resized to best fit in the given width.

## Security
This package contains some elements to manage a simple cipher mechanism. It allows to encrypt and decrypt packs of bytes using MD5. 

## Primitive utils
This class offers access to primitive values read or write. This functionality is in alpha stage.
- `public static Object getSimpleObject(Class<?> targetClass, String valueText)` that will return an object of the primitive type with the value corresponding to the string content - if that fits.
- `public static Object getSimpleObjectDefaultValue(Class<?> targetClass, String valueText)` that will return an object of the primitive type with a default value.
and other methods...

## File utilities
This class offers methods acting on files to simplify using them.
- `public static void copy(InputStream source, OutputStream target) throws IOException` copy an input stream to an output stream.
- `public static File createTemporaryDirectory(String key) throws IOException` creates a temporary directory that will be automatically deleted on exit.
- `public static void markForDeletionOnExit(File file)` mark a directory or file for deletion on exit. Wipes all the content, recursively, for a directory.
- `public static void deleteDirectory(File file)` delete a directory and all its content, recursively.
- `public static List<String> readAllLines(String filepath)` read all content of an ASCII file, given its path, as a list of Strings.
- `public static List<String> readAllLines(File file)` read all content of an ASCII file as a list of Strings.
- `public static List<String> readAllLines(InputStream stream)` read all content of an ASCII input stream as a list of Strings.
- `public static List<String> readAllLines(Reader reader)` read all content of an ASCII reader as a list of Strings.

## ResourceManager
This class is meant to simplify and buffer resources.

### Fonts
- `public static Font getFont(String name, int height, int style)` gets the font with the given family, height and style. It stores it for faster later calls.
- `public static Font getBoldFont(Font baseFont)` gets the bold font corresponding to the given font. It stores it for faster later calls.
- `public static Font getItalicFont(Font baseFont)` gets the italic font corresponding to the given font. It stores it for faster later calls.
- `public static Font getBoldItalicFont(Font baseFont)` gets the bold italic font corresponding to the given font. It stores it for faster later calls.

### Cursors
- `public static Cursor getCursor(int id)` Gets the system cursor.

### Images
- `public static Image getImage(String path)` loads an image from the given file path, either on disk on in jars. Returns the "missing image" image if unable to load.
- `private static Image getMissingImage()` returns a 10x10 black image.

## Icons
- `public static ImageIcon getIcon(Image image)` converts the image as an Icon.
- `public static ImageIcon getIcon(String path)` converts the image found at the given path( file on disk or in jar) as an Icon.
- `public static ImageIcon getIcon(String path, int width)` converts the image - resized to given width - found at the given path( file on disk or in jar) as an Icon.

## StringUtils
Set of utilities for Strings. There currently is only one.
- `public static String[] intelligentSplit(String string)` split a String, consider the separation between the tokens with blank spaces, unless the part of the text is between double quotes.

## Utils
This class offers various utilities. These functionalities are still in alpha stage and will be described as soon as they are more tested.

### Parsing arguments
These methods are using org.apache.commons.cli functions to parse command line arguments.
You should use annotations to define the associations between the target class fields and the names of arguments in the command line. The annotations are described below.

- `public static void parseArguments(Object target, String[] args) throws IllegalArgumentException, IllegalAccessException, ParseException` analyze the given arguments and fills the target object. This method may throw exceptions if an error occurs.
- `public static boolean securedParseArguments(Object target, String[] args)` analyze the given arguments and fills the target object. It does not throw exceptions but returns a boolean. False means that an error occurs, True if all is OK.
- `public static void parseArguments(Class<X> cl, String[] args)` analyze the given arguments, creates a new object of the given class and fills the object.

There are three annotations to be used while parsing arguments.
`@ArgumentName(String value)` is mandatory. If a field does not have it set, it will not be considered as a valid possible argument. The value is not mandatory : if absent, the field's name will be used as the ragument's name.
`@ArgumentShortName(String value)` may be added to add a short argument name. It is optional.
`@ArgumentDescription(String value)` may be added to add an argument description. It is optional.