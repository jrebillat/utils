# Utils
This package contains - and will contain more - utilities classes used by the other Alantea packages.

## Messages
A common problem is to manage internationalization in Strings, either for GUI, logging, file output or console output. The Java response is to offer access to properties files that contains key-values pairs of Strings and that may be internationalized, naming it a *bundle*.

Most of the time, a package has its own bundle, stored internally, that manage the strings for this package. This is good, as far as a package will not offer packages using the packages functionalities to modify its personal strings. In this case, the higher level package must have access to the internal bundle.

To offer such a possibility, but controlled, the Utils package offers a MultiMessages class that is able to manage loading and accessing multiple bundles at once and thus offers all packages to access all of the stored keys, in any bundle.

One problem is the priority of bundles between them. As it is not possible to define which bundle will be loaded before another, it is - for the moment - left to chance. But the messaging class will manage two special cases : the first bundle to search in and the latest bundle to search - the catch-all case.

Bundles are technically named files contained in the class path (in the src package hierarchy, in any resources directory or in a jar). The bundle may be found using a java-like name (package.name) or through a class that will have the same signature as the bundle files.

Applications may also need to manage those elements in different ways (not using bundles). We offer here some approaches for such management.

### First bundle
The first bundle has to be set by the application. This one will be search before any other bundle. Its keys will thus override all other definitions of the same keys in other bundles.

### latest bundle
The latest bundle may to be set by the application or a package. Be careful : there is just one latest package. Defining a latest package will forget the previously defined one. This bundle will be search after any other bundle. Its keys may thus be overridden by any other definitions of the same keys in other bundles.

### Applicative keys
The applicative keys are key/value pairs managed like the bundle's keys, and thus accessed as if they were in a bundle that is searched before the first bundle. These values are not internationalized but may be calculated by the application and even modified during execution.

### Providers
A message provider in a class that offers same capabilities as bundles : searching for a key corresponding value depending on the locale language. Those classes are created and managed in the application and are searched just after the first bundle.

## MultiMessages
The MultiMessages class offers the following static methods for standard bundles use :
- `public static boolean addBundle(String path)` will add the bundle in the bundles pool.
- `public static boolean addAssociatedBundle(Object associated)` will add the bundle which name and package are the same as the given object class.
- `public static void setFirstBundle(String path)` will set the bundle as first bundle. It has not to have been added before.
- `public static void setLatestBundle(String path)` will set the bundle as latest bundle. It has not to have been added before.
- `public static String get(String key, String... args)` will search for the key in the known bundles, starting by first bundle and ending with latest. It will return the found value or the key if no value has been found. It will search the value for patterns like [1] or [2],... and replace them by the corresponding argument if any has been given (arguments numbers are starting at 1).

There are also methods for special accesses (applicative keys and providers):
- `public static void addApplicativeKey(String key, String value)` will add the key value in the keys pool.
- `public static void removeApplicativeKey(String key)` will remove the key value from the keys pool.
- `public static void addProvider(MessageProvider provider)` will add the provider in the providers pool.

In special cases, the coder wants to manage himself the bundle for his code. There are some methods to do that :
- `public static ResourceBundle getBundle(String path)` will add the bundle in the bundles pool.
- `public static ResourceBundle getBundle(Class reference)` will add the bundle which name and package are the same as the given class in the bundles pool.
- `public static ResourceBundle getBundle(Object object)` will add the bundle which name and package are the same as the given object class in the bundles pool.

## MessageProvider
A message provider is a class deriving from the MessageProvider class. They have to implement methods in the form of 
`public String get_fr_FR(String key, String... args)` with the method names containing the actual local they manage.

A default `public String get(String key, String... args)` must exist to manage unknown locales.

Note: this functionality is in alpha stage.

## LocalizedString
The LocalizedString class will simplify handling of internationalization in Strings. It is built with :
- `public LocalizedString(String key, String... args)` and will contains the localized value of the key, as found in the bundles.

The contained method is :
- `public String getKey()` will return the key.

The class also override the `toString`, `compare`, `equals` and `hashCode` methods.

## Exception management
The LntException is there to be the base class for all exceptions in the Alantea packages. It should be derived or directly used in other packages

### LntException
Such an exception may be created with :
- `public LntException(String message)`
- `public LntException(String message, Throwable source)`

The last 10 created LntExceptions - not only the ones that were thrown - are automatically saved in a list.
The exceptions list may be got with the static method :
- `public static List<LntException> popExceptions()` that will return the list of LnTExceptions thrown since the last call, and clear the list of exceptions.

##Color names
The ColorNames class gives simplified access to standard color names, web codes and AWT Color values.The methods are:
- `public static String getColorCode(String name)` that will return the web color code that corresponds to the given color name.
- `public static Color getColor(String name)` that will return the AWT Color object corresponding to the given name.
- `public static List<String> getColorNames()` that will return the list of known color names.
- `public static void addColor(String name, String code)` that will add a new name and associate a web color code to it.
- `public static void changeColor(String name, String code)` that will change the web color code associated to a given color name.

## Primitive utils
This class offers access to primitive values read or write. This functionality is in alpha stage.

## Utils
This class will offer various utilities. This functionality is in alpha stage.
